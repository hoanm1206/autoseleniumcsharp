﻿using AutoSelenium.Database;
using AutoSelenium.Model.DatabaseModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AutoSelenium
{
    /// <summary>
    /// Interaction logic for ChromeSettingWindow.xaml
    /// </summary>
    public partial class ChromeSettingWindow : UserControl
    {
        private ProfileChrome CurrentProfileChrome = null;
        public ChromeSettingWindow()
        {
            InitializeComponent();
            CreateDbIfNotExist();
            ShowData();
            //ReadChromeProfiles();
            //InsertChromeProfile();
        }

        private void ShowData(List<ProfileChrome> searchList = null)
        {
            if (searchList != null)
            {
                listViewProfile.ItemsSource = searchList;
            }
            else
            {
                listViewProfile.ItemsSource = ReadChromeProfiles();
            }
            GridView gv = listViewProfile.View as GridView;
            var grvColumns = gv.Columns;
            foreach (GridViewColumn column in grvColumns)
            {
                column.Width = column.ActualWidth;
                column.Width = double.NaN;
            }
        }

        static void CreateDbIfNotExist()
        {
            try
            {
                if (!File.Exists("ChromeProfileDB.db"))
                {
                    var dbName = "ChromeProfileDB.db";
                    var dbPath = new FileInfo(dbName).FullName;
                    using var connection = new SQLiteConnection("Data Source=" + dbPath + "; Version = 3;");
                    connection.Open();
                    using (SQLiteCommand fmd = connection.CreateCommand())
                    {
                        fmd.CommandText = File.ReadAllText("D:\\ChromeProfileDB.sql");
                        fmd.ExecuteNonQuery();
                    }
                    connection.Close();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private void InsertChromeProfile()
        {
            using var dbContext = new ChromeProfileDbContext();
            var profileChrome = new ProfileChrome();

            ReadChromeProfiles();
        }

        private List<ProfileChrome> ReadChromeProfiles()
        {
            using var dbContext = new ChromeProfileDbContext();
            var profiles = dbContext.ProfileChrome.ToList();
            profiles.ForEach(prf => prf.PrintInfor());
            return profiles;
        }

        static void CretateDbNormal()
        {
            using (var connection = CreateConnection())
            {
                connection.Open();
                SQLiteCommand command = connection.CreateCommand();
                command.CommandText = "select * from ProfileDetails";
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Trace.WriteLine(reader.GetString(1));
                    }
                }
            }
        }
        static SQLiteConnection CreateConnection()
        {
            var dbName = "ChromeProfileDB.db";
            var dbPath = new FileInfo(dbName).FullName;
            SQLiteConnection sqlite_conn = null;
            try
            {
                if (!File.Exists("ChromeProfileDB.db"))
                {
                    sqlite_conn = new SQLiteConnection("Data Source=" + dbPath + "; Version = 3;");
                    sqlite_conn.Open();
                    using (SQLiteCommand fmd = sqlite_conn.CreateCommand())
                    {
                        fmd.CommandText = File.ReadAllText("D:\\ChromeProfileDB.sql");
                        fmd.ExecuteNonQuery();
                    }
                    sqlite_conn.Close();
                }
                else
                {
                    sqlite_conn = new SQLiteConnection("Data Source=" + dbPath + "; Version = 3;");
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            return sqlite_conn;
        }

        private void FillDataProfile(ProfileChrome profileChrome)
        {
            TbProfileName.Text = profileChrome.ProfileName;
            TbTweeterName.Text = profileChrome.TweeterName;
            TbGmailAddress.Text = profileChrome.GmailAddress;
            TbPhoneNumber.Text = profileChrome.PhoneNumber;
            TbUserAgent.Text = profileChrome.UserAgent;
            TbBSCWalletContract.Text = profileChrome.Wallet;
            TbProxy.Text = profileChrome.Proxy;
        }

        private void CheckBoxChanged(object sender, RoutedEventArgs e)
        {

        }

        private void ListViewItem_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void listViewProfile_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sender != null)
            {
                var profileChrome = (sender as ListView).SelectedItem as ProfileChrome;
                if (profileChrome != null)
                {
                    CurrentProfileChrome = profileChrome;
                    FillDataProfile(profileChrome);
                }
            }
        }

        private void OnClickInsert(object sender, RoutedEventArgs e)
        {
            var profileName = TbProfileName.Text;
            var proxy = TbProxy.Text;
            var tweeterName = TbTweeterName.Text;
            var gmailAddress = TbGmailAddress.Text;

            var phoneNumber = TbPhoneNumber.Text;
            var userAgent = TbUserAgent.Text;
            var wallet = TbBSCWalletContract.Text;

            using var dbContext = new ChromeProfileDbContext();
            ProfileChrome profile = new ProfileChrome();
            profile.ProfileName = profileName;
            profile.TweeterName = tweeterName;
            profile.PhoneNumber = phoneNumber;
            profile.GmailAddress = gmailAddress;
            profile.UserAgent = userAgent;
            profile.Wallet = wallet;
            profile.Proxy = proxy;
            profile.Status = "REST";
            dbContext.ProfileChrome.Add(profile);
            try
            {
                dbContext.SaveChanges();
                MessageBox.Show("Success!");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Thông tin profile bị trùng lặp!");
            }
            
          
            ShowData();
        }

        private bool AddNewWalletsRecord(ChromeProfileDbContext dbContext)
        {
            try
            {
                return dbContext.SaveChanges() > 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Chèn dữ liệu không thành công, Dữ liệu Contract, Private Key, Proxy ,Twitter Infor, User Agent không được trùng!");
            }
            return false;
        }

        private void OnClickEdit(object sender, RoutedEventArgs e)
        {
            if (CurrentProfileChrome == null)
            {
                MessageBox.Show("Bản ghi không tồn tại!");
                return;
            }
            var profileName = TbProfileName.Text;
            var proxy = TbProxy.Text;
            var tweeterName = TbTweeterName.Text;
            var gmailAddress = TbGmailAddress.Text;

            var phoneNumber = TbPhoneNumber.Text;
            var userAgent = TbUserAgent.Text;
            var wallet = TbBSCWalletContract.Text;

            using var dbContext = new ChromeProfileDbContext();

            var profile = CurrentProfileChrome;
            profile.ProfileName = profileName;

            profile.TweeterName = tweeterName;
            profile.PhoneNumber = phoneNumber;
            profile.GmailAddress = gmailAddress;
            profile.Proxy = proxy;
            profile.UserAgent = userAgent;
            profile.Status = CurrentProfileChrome.Status;
            profile.Wallet = wallet;
            dbContext.ProfileChrome.Update(CurrentProfileChrome);
            try
            {
                dbContext.SaveChanges();
                MessageBox.Show("Success!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            ShowData();
        }

        private void OnClickDelete(object sender, RoutedEventArgs e)
        {
            if (CurrentProfileChrome != null)
            {
                using var dbContext = new ChromeProfileDbContext();
                dbContext.ProfileChrome.Remove(CurrentProfileChrome);
                try
                {
                    MessageBoxResult result = MessageBox.Show("Xác nhận xóa bản ghi này ?", "Delete Record Dialog", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
                    if (result == MessageBoxResult.Yes)
                    {
                        dbContext.SaveChanges();
                        MessageBox.Show("Success!");
                        ShowData();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Xóa không thành công!");
                }
            }
            else
            {
                MessageBox.Show("Bản ghi không tồn tại!");
            }
        }

        private void OnClickSearch(object sender, RoutedEventArgs e)
        {
            var str = TbSearch.Text;
            using var dbContext = new ChromeProfileDbContext();
            var searchList = (from profile in dbContext.ProfileChrome
                              where profile.ProfileName.Contains(str) ||
                              profile.TweeterName.Contains(str) ||
                              profile.Wallet.Contains(str) ||
                              profile.GmailAddress.Contains(str) ||
                              profile.PhoneNumber.Contains(str) ||
                              profile.Status.Contains(str)
                              select profile).ToList();
            if (searchList.Count > 0)
            {
                ShowData(searchList);
            }
        }
    }
}
