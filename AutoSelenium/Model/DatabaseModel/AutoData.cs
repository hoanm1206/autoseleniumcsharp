﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoSelenium.Model.DatabaseModel
{
    [Table("AutoData")]
    public class AutoData
    {
        [Key]
        public int Id { get; set; }
        public string ChromePortablePath { get; set; }
        public string Link { get; set; }
        public string Cookie { get; set; }
        public bool UseCookie { get; set; }
        public string ProxyLoginId { get; set; }
        public string ProxyLoginPassword { get; set; }
        public string CaptchaKey { get; set; }

        [NotMapped]
        public System.Drawing.Size windowSize { get; set; }

        [NotMapped]
        public string secretCode { get; set; }
    }
}
