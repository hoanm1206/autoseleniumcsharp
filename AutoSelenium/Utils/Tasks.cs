﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AutoSelenium
{
    public class Tasks
    {
        public static CancellationTokenSource CancellationTokenSource = new CancellationTokenSource();
        public static void StartAndWaitAllThrottled(IEnumerable<Task> tasksToRun, int maxActionsToRunInParallel, CancellationToken cancellationToken = new CancellationToken())
        {
            StartAndWaitAllThrottled(tasksToRun, maxActionsToRunInParallel, -1, cancellationToken);
        }

        public static async void StartAndWaitAllThrottled(IEnumerable<Task> tasksToRun, int maxActionsToRunInParallel, int timeoutInMilliseconds, CancellationToken cancellationToken = new CancellationToken())
        {
            // Convert to a list of tasks so that we don't enumerate over it multiple times needlessly.
            var tasks = tasksToRun.ToList();

            using (var throttler = new SemaphoreSlim(maxActionsToRunInParallel))
            {
                var postTaskTasks = new List<Task>();

                // Have each task notify the throttler when it completes so that it decrements the number of tasks currently running.
                tasks.ForEach(t => postTaskTasks.Add(t.ContinueWith(tsk => throttler.Release(), CancellationTokenSource.Token)));

                // Start running each task.
                foreach (var task in tasks)
                {
                    // Increment the number of tasks currently running and wait if too many are running.
                    throttler.Wait(timeoutInMilliseconds, cancellationToken);
                    CancellationTokenSource.Token.ThrowIfCancellationRequested();
                    task.Start();
                }
                try
                {
                     await Task.WhenAll(postTaskTasks.ToArray());
                }
                catch (Exception e)
                {
                    
                }
            }
        }
    }

    public class TaskPool
    {
        private readonly HashSet<IInternalTask> workingTasks = new HashSet<IInternalTask>();
        private readonly ConcurrentQueue<IInternalTask> queue = new ConcurrentQueue<IInternalTask>();
        private readonly object tasksMutex = new object();
        private readonly object checkMutex = new object();

        private int threadsMaxCount;

        private interface IInternalTask
        {
            Task Execute();
        }

        private class InternalTaskHolder : IInternalTask
        {
            public Func<Task> Task { get; set; }
            public TaskCompletionSource<IDisposable> Waiter { get; set; }

            public async Task Execute()
            {
                await Task();
                Waiter.SetResult(null);
            }
        }

        private class InternalTaskHolderGeneric<T> : IInternalTask
        {
            public Func<Task<T>> Task { get; set; }
            public TaskCompletionSource<T> Waiter { get; set; }

            public async Task Execute()
            {
                var result = await Task();
                Waiter.SetResult(result);
            }
        }


        /// <summary>
        /// Raised when all tasks have been completed.
        /// </summary>
        public event EventHandler Completed;

        /// <summary>
        /// Creates a new thread queue with a maximum number of threads
        /// </summary>
        /// <param name="threadsMaxCount">The maximum number of currently threads</param>
        public TaskPool(int threadsMaxCount)
        {
            this.threadsMaxCount = threadsMaxCount;
        }

        /// <summary>
        /// Creates a new thread queue with a maximum number of threads and the tasks that should be executed.
        /// </summary>
        /// <param name="threadsMaxCount">The maximum number of currently threads.</param>
        /// <param name="tasks">The tasks that will be execut in pool.</param>
        public TaskPool(int threadsMaxCount, Func<Task>[] tasks) : this(threadsMaxCount)
        {
            foreach (var task in tasks)
            {
                queue.Enqueue(new InternalTaskHolder { Task = task });
            }
        }


        /// <summary>
        /// Adds a task and runs it if free thread exists. Otherwise enqueue.
        /// </summary>
        /// <param name="task">The task that will be execut</param>
        public Task Enqueue(Func<Task> task)
        {
            lock (tasksMutex)
            {
                var holder = new InternalTaskHolder { Task = task, Waiter = new TaskCompletionSource<IDisposable>() };

                if (workingTasks.Count >= threadsMaxCount)
                    queue.Enqueue(holder);
                else
                    StartTask(holder);

                return holder.Waiter.Task;
            }
        }

        /// <summary>
        /// Adds a task and runs it if free thread exists. Otherwise enqueue.
        /// </summary>
        /// <param name="task">The task that will be execut</param>
        public Task<T> Enqueue<T>(Func<Task<T>> task)
        {
            lock (tasksMutex)
            {
                var holder = new InternalTaskHolderGeneric<T> { Task = task, Waiter = new TaskCompletionSource<T>() };

                if (workingTasks.Count >= threadsMaxCount)
                    queue.Enqueue(holder);
                else
                    StartTask(holder);

                return holder.Waiter.Task;
            }
        }

        /// <summary>
        /// Starts the execution of a task.
        /// </summary>
        /// <param name="task">The task that should be executed.</param>
        private async void StartTask(IInternalTask task)
        {
            await task.Execute();
            TaskCompleted(task);
        }

        private void TaskCompleted(IInternalTask task)
        {
            lock (tasksMutex)
            {
                workingTasks.Remove(task);
                CheckQueue();

                if (queue.Count == 0 && workingTasks.Count == 0)
                    OnCompleted();
            }
        }

        /// <summary>
        /// Checks if the queue contains tasks and runs as many as there are free execution slots.
        /// </summary>
        private void CheckQueue()
        {
            lock (checkMutex)
                while (queue.Count > 0 && workingTasks.Count < threadsMaxCount)
                    if (queue.TryDequeue(out IInternalTask task))
                        StartTask(task);
        }

        /// <summary>
        /// Raises the Completed event.
        /// </summary>
        protected void OnCompleted()
        {
            Completed?.Invoke(this, null);
        }
    }
}
