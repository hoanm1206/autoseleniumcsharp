﻿using AutoSelenium.Model.DatabaseModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoSelenium.Database
{
    public class ChromeProfileDbContext : DbContext
    {
        private const string dbName = "ChromeProfileDB.db";

        public DbSet<ProfileChrome> ProfileChrome { get; set; }
        public DbSet<AutoData> AutoData { get; set; }
      

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var dbPath = new FileInfo(dbName).FullName;
            var connectionString = "Data Source="+dbPath+ ";foreign keys=true;";
            optionsBuilder.UseSqlite(connectionString);
        }
    }

}
