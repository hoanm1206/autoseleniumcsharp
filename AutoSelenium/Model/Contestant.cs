﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoSelenium.Model
{
    class Contestant
    {
        public string campaign_key { get; set; }
        public ContestantItem contestant { get; set; }
        public bool additional_details { get; set; }

    }

    class ContestantItem
    {
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string email { get; set; }
        public string name { get; set; }
        public string competition_subscription { get; set; }
        public string date_of_birth { get; set; }
        public string stored_dob { get; set; }
        public bool send_confirmation { get; set; }
        public string bep20_wallet_address { get; set; }
        public string twitter_account { get; set; }
        public string telegram_account { get; set; }


    }
}
