﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoSelenium.Model.DatabaseModel
{
    [Table("ProfileChrome")]
    public class ProfileChrome :Bindable
    {
        [Key]
        [Required]
        public int Id { get; set; }
        [Required]
        [Column("ProfileName")]
        public string ProfileName { get; set; }
        [Required]
        [Column("TwitterUserName")]
        public string TweeterName { get; set; }      
        [Required]
        [Column("PhoneNumber")]
        public string PhoneNumber { get; set; }
        [Required]
        [Column("GmailAddress")]
        public string GmailAddress { get; set; }
        
        [Column("UserAgent")]
        public string UserAgent { get; set; }

        [Column("Wallet")]
        public string Wallet { get; set; }
       
        [Column("Status")]
        public string Status { get; set; }

        [Column("Proxy")]
        public string Proxy { get; set; }

        public void PrintInfor() => Trace.WriteLine($"Profile {Id} - {ProfileName} - {TweeterName} - {PhoneNumber} - {GmailAddress} - {Status}");
    }
}
