﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AutoSelenium.Utils
{
    public static class Const
    {
        public enum ProcessStatus
        {
            REST,
            PROCESSING,
            PROCESSING_CAPTCHA,
            SUCCESS,
            FAILED
        }
        public enum CaptchaType
        {
            NORMAL_CAPTCHA,
            RE_CAPTCHA_V2,
            RE_CAPTCHA_V3,
            H_CAPTCHA,
        }

        public class DataTrackEvent
        {
            public const string FOLLOW_TWITTER = "###APP_NAME### Click|twitter|follow";
            public const string RETWEET_TWITTER = "###APP_NAME### Click|twitter|retweet";
            public const string TWEET_TWITTER = "###APP_NAME### Click|twitter|tweet";
            public const string TWEET_HASH_TAG_TWITTER = "###APP_NAME### Click|twitter|hashtags";
            public const string JOIN_DISCORD_SERVER = "###APP_NAME### Click|discord|join_server";
            public const string SUBMIT_URL = "###APP_NAME### Click|submit|url";
            public const string SHARE_ACTION = "###APP_NAME### Click|share|action";
            public const string SECRET_CODE = "###APP_NAME### Click|secret|code";
            public const string SUBSCRIBE_EMAIL = "###APP_NAME### Click|email|subscribe";
            public const string LOYALTY = "###APP_NAME### Click|loyalty|loyalty";
        }
        public enum TaskType
        {
            VISIT_WEBISITE,
            FOLLOW_TWITTER,
            FOLLOW_US_ON_TWITTER,
            FOLLOW_MEDIUM,
            RETWEET_TWITTER,
            JOIN_TELEGRAM_GROUP,
            JOIN_TELEGRAM_CHANNEL,
            JOIN_DISCORD,
            JOIN_OUR_DISCORD_SERVER,
            VISIT_INSTAGRAM,
            VISIT_YOUTUBE,
            VISIT_REDDIT,
            VISIT_FACEBOOK,
            REFER_FRIEND,
            VOTE_FOR,
            GET_BONUS_ENTRY_FOR_EVERYTHING,
            BNB_WALLET,
            SUBMIT_BSC_WALLET_ADDRESS,
            JOIN_METAVERSE_TELEGRAM_COMMUNITY,
            JOIN_METAVERSE_TELEGRAM_CHANNEL,
            JOIN_TELEGRAM,
            JOIN_DISCORD_COMMUNITY,
            GET_A_BONUS_WHEN_YOU_COMPLETE_EVERYTHING,
            lIKE_FACEBOOK,
            SHARE_OUR_CAMPAIGN_WITH_FRIEND,
            RETWEET_THIS_TWEET_AND_TAG_FRIEND,
            TWEET_ON_TWITTER,
            VISIT,
            ENTER_SECRET_CODE,
            ADD_TO_WATCH_LIST,
            TELEGRAM,
            DISCORD,
            RETWEET_NOT_TAG,
            FOLLOW_TWEETER_COMMON,
            RETWEET_AND_TAG,
            VISIT_INSTAGRAM_COMMON,
            UNKNOW
        }
        public class TaskRegex
        {
            public static string VISIT_WEBISITE = "^(Visit) .* (website)$";
            public static string VISIT_INSTAGRAM = "^(Visit) .* (on) (Instagram)$";
            public static string VISIT_YOUTUBE = "^(Visit) .* (on) (YouTube)$";
            public static string VISIT_REDDIT = "^(Visit) .* (on) (Reddit)$";
            public static string VISIT_FACEBOOK = "^(Visit) .* (on) (Facebook)$";
            public static string VISIT = "^(Visit) .*$";
            public static string FOLLOW_TWITTER = "^(Follow) .* (on) (Twitter)$";
            public static string FOLLOW_US_ON_TWITTER = "^(Follow Us On) .* (Twitter)$";
            public static string FOLLOW_MEDIUM = "^(Follow) .* (on) (Medium)$";
            public static string RETWEET_TWITTER = "^(Retweet) .* (on) (Twitter)$";
            public static string JOIN_TELEGRAM_GROUP = "^(Join) .* (Telegram) (Group)$";
            public static string JOIN_TELEGRAM_CHANNEL = "^(Join) .* (Telegram) (Channel)$";
            public static string JOIN_TELEGRAM = "^(Join) .* (Telegram)$";
            public static string JOIN_DISCORD_COMMUNITY = "^(Join) .* (Community)$";
            public static string JOIN_DISCORD = "^(Join) .* (Discord)$";
            public static string JOIN_OUR_DISCORD_SERVER = "^(Join Our Discord Server)$";
            public static string REFER_FRIEND = "Refer Friends For Extra Entries";
            public static string VOTE_FOR = "^(Vote for) .*";
            public static string GET_BONUS_ENTRY_FOR_EVERYTHING = "^(Get .* Bonus Entries For Completing Everything)$";
            public static string GET_A_BONUS_WHEN_YOU_COMPLETE_EVERYTHING = "^(Get A Bonus When You Complete Everything)$";
            public static string BNB_WALLET = "^(BNB wallet BSC for the) .* (whitelist)$";
            public static string SUBMIT_BSC_WALLET_ADDRESS = "^(Submit BSC[(]BEP20[)] Wallet Address)$";
            public static string DOWLOAD_WALLET = "^(Download) .* (Wallet (Android/iOS))$";
            public static string SCREENSHOOT_WALLET = "^(Screenshot) .* (Wallet on your mobile device)$";
            public static string JOIN_METAVERSE_TELEGRAM_COMMUNITY = "^(Join) .* (Metaverse Telegram Community)$";
            public static string JOIN_METAVERSE_TELEGRAM_CHANNEL = "^(Join) .* (Metaverse Channel)$";
            public static string TWEET_ON_TWITTER = "^(Tweet on Twitter)$";
            public static string RETWEET_THIS_TWEET_AND_TAG_FRIEND = "^(Retweet this tweet and tag) .* (friends)$";
            public static string lIKE_FACEBOOK = "^(Like) .* (Facebook)$";
            public static string ENTER_SECRET_CODE = "^(Enter the Secret Code)$";
            public static string SHARE_OUR_CAMPAIGN_WITH_FRIEND = "^(Share Our Campaign With Friends) .*";
            public static string ADD_TO_WATCH_LIST = "^(Add)(?:.*Watchlist)";
            public static string TELEGRAM = ".* (Telegram).*";
            public static string DISCORD = ".* (Discord).*";
            public static string RETWEET_NOT_TAG = "^(Retweet)(?!.*(tag|Tag))";
            public static string RETWEET_AND_TAG = "^(Retweet)(?:.*(tag|Tag))";
            public static string FOLLOW_TWEETER_COMMON = "^(Follow)(?:.*Tweeter)";
            public static string VISIT_INSTAGRAM_COMMON = "^(Visit)(?:.*(Instagrams))";
        }
    }
}
