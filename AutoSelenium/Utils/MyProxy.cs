﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Packaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoSelenium.Utils
{
    public class MyProxy
    {
        public const long BUFFER_SIZE = 4096;
        public static string FILE_PATH = Environment.CurrentDirectory + "\\" + "proxy_auth_plugin.crx";
        public static string CreateProxyExtension(string host, string port, string user, string password)
        {
            string manifest_json = "{\n" +
                "  \"version\": \"1.0.0\",\n" +
                "  \"manifest_version\": 2,\n" +
                "  \"name\": \"Chrome Proxy\",\n" +
                "  \"permissions\": [\n" +
                "    \"proxy\",\n" +
                "    \"tabs\",\n" +
                "    \"unlimitedStorage\",\n" +
                "    \"storage\",\n" +
                "    \"<all_urls>\",\n" +
                "    \"webRequest\",\n" +
                "    \"webRequestBlocking\"\n" +
                "  ],\n" +
                "  \"background\": {\n" +
                "    \"scripts\": [\"background.js\"]\n" +
                "  },\n" +
                "  \"minimum_chrome_version\":\"22.0.0\"\n" +
                "}";
            String background_js = String.Format("var config = {{\n" +
                "  mode: \"fixed_servers\",\n" +
                "  rules: {{\n" +
                "    singleProxy: {{\n" +
                "      scheme: \"http\",\n" +
                "      host: \"{0}\",\n" +
                "      port: parseInt({1})\n" +
                "    }},\n" +
                "    bypassList: [\"localhost\"]\n" +
                "  }}\n" +
                "}};\n" +
                "\n" +
                "chrome.proxy.settings.set({{value: config, scope: \"regular\"}}, function() {{}});\n" +
                "\n" +
                "function callbackFn(details) {{\n" +
                "return {{\n" +
                "authCredentials: {{\n" +
                "username: \"{2}\",\n" +
                "password: \"{3}\"\n" +
                "}}\n" +
                "}};\n" +
                "}}\n" +
                "\n" +
                "chrome.webRequest.onAuthRequired.addListener(\n" +
                "callbackFn,\n" +
                "{{urls: [\"<all_urls>\"]}},\n" +
                "['blocking']\n" +
                ")}};", new string[] { host, port, user, password });
            if (!File.Exists(FILE_PATH))
            {
                WriteToFile("manifest.json", manifest_json);
                WriteToFile("background.js", background_js);
                AddFileToZip("proxy_auth_plugin.crx", "manifest.json");
                AddFileToZip("proxy_auth_plugin.crx", "background.js");
            }
            return FILE_PATH;
        }

        public static void WriteToFile(string fileName, string text)
        {
            try
            {
                string filePath = Environment.CurrentDirectory + "\\" + fileName;
                if (!File.Exists(filePath))
                {
                    File.AppendAllText(filePath, text);
                }
            }
            catch (Exception e) { }
        }
        public static void AddFileToZip(string zipFilename, string fileToAdd)
        {
            using (Package zip = Package.Open(zipFilename, FileMode.OpenOrCreate))
            {
                string destFilename = ".\\" + Path.GetFileName(fileToAdd);
                Uri uri = PackUriHelper.CreatePartUri(new Uri(destFilename, UriKind.Relative));
                if (zip.PartExists(uri))
                {
                    zip.DeletePart(uri);
                }
                PackagePart part = zip.CreatePart(uri, "", CompressionOption.Normal);
                using (FileStream fileStream = new FileStream(fileToAdd, FileMode.Open, FileAccess.Read))
                {
                    using (Stream dest = part.GetStream())
                    {
                        CopyStream(fileStream, dest);
                    }
                }
            }
        }

        public static void CopyStream(global::System.IO.FileStream inputStream, global::System.IO.Stream outputStream)
        {
            long bufferSize = inputStream.Length < BUFFER_SIZE ? inputStream.Length : BUFFER_SIZE;
            byte[] buffer = new byte[bufferSize];
            int bytesRead = 0;
            long bytesWritten = 0;
            while ((bytesRead = inputStream.Read(buffer, 0, buffer.Length)) != 0)
            {
                outputStream.Write(buffer, 0, bytesRead);
                bytesWritten += bytesRead;
            }
        }
    }
}
