﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoSelenium.Model
{
    public class GleamTaskElement
    {
        public WebElement buttonEntryElement { get; set; }
        public WebElement followElement { get; set; }
        public WebElement continueElement { get; set; }
    }
}
