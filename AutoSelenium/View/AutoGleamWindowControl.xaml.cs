﻿using AutoSelenium.Database;
using AutoSelenium.Model;
using AutoSelenium.Model.DatabaseModel;
using AutoSelenium.Utils;
using Leaf.xNet;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using TwoCaptcha.Captcha;
using TwoCaptcha.Exceptions;
using Xunit;
using static AutoSelenium.Utils.Const;
using TimeoutException = TwoCaptcha.Exceptions.TimeoutException;

namespace AutoSelenium
{
    /// <summary>
    /// Interaction logic for AutoGleamWindowControl.xaml
    /// </summary>
    public partial class AutoGleamWindowControl : UserControl
    {
        private List<Profile> listProfile = new List<Profile>();
        private TwoCaptcha.TwoCaptcha solver = null;
        private CaptchaType captchaType;
        private Process cmd = null;
        private volatile int chromePosition = 0;
        private int threadCount = 0;
        private string template = "";
        private List<Task> listOfTasks = new List<Task>();
        private AutoData autoData = null;
        private string linkReferer = "";
        private NetworkAuthenticationHandler networkAuthenticationHandler;
        public AutoGleamWindowControl()
        {
            CreateDbIfNotExist();
            InitializeComponent();
            InitializeCaptchaSolver();
            ShowProfileList();
            ShowAutoData();
            CreateNetWorkCredental();
        }

        private void InitializeCaptchaSolver()
        {
            solver = new TwoCaptcha.TwoCaptcha(editKeyCaptcha.Text);
            solver.RecaptchaTimeout = 600;
            solver.PollingInterval = 5;
            captchaType = CaptchaType.H_CAPTCHA;
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            threadCount = Int32.Parse(edtThreadCount.Text);
            linkReferer = editlink.Text;
            if (!String.IsNullOrEmpty(linkReferer))
            {
                var widthChromePosition = 0;
                var yChromePosition = 0;
                var tab = 0;

                var taskRuning = listOfTasks.FirstOrDefault(task => task.IsCompleted.Equals(false));
                if (taskRuning != null)
                {
                    MessageBox.Show("Chrome đang được sử dụng!");
                    return;
                }
                else
                {
                    listOfTasks.Clear();
                    Tasks.CancellationTokenSource = new CancellationTokenSource();
                }
                var currentPosition = 0;
                foreach (Profile item in listProfile)
                {
                    if (item.IsSelected)
                    {
                        item.CurrentPosition = currentPosition;
                        if (currentPosition < threadCount)
                        {
                            currentPosition++;
                            if (currentPosition == threadCount)
                            {
                                currentPosition = 0;
                            }
                        }
                    }
                }
                foreach (Profile item in listProfile)
                {
                    if (item.IsSelected)
                    {
                        item.WindownWidth = 1;
                        item.WindownHeight = 1;
                        if (threadCount == 2)
                        {
                            item.XPosition = item.CurrentPosition;
                            item.YPosition = 0;
                            item.WindownWidth = 2;
                            item.WindownHeight = 1;
                        }
                        else if (threadCount == 3)
                        {
                            item.XPosition = item.CurrentPosition;
                            item.YPosition = 0;
                            item.WindownWidth = 3;
                            item.WindownHeight = 1;
                        }
                        else if (threadCount == 4)
                        {
                            if (item.CurrentPosition == 0 || item.CurrentPosition == 1)
                            {
                                item.YPosition = 0;
                                item.XPosition = item.CurrentPosition;
                            }
                            else if (item.CurrentPosition == 2 || item.CurrentPosition == 3)
                            {
                                item.YPosition = 1;
                                item.XPosition = item.CurrentPosition - 2;
                            }
                            item.WindownWidth = 2;
                            item.WindownHeight = 2;
                        }
                        else if (threadCount == 6)
                        {
                            if (item.CurrentPosition == 0 || item.CurrentPosition == 1 || item.CurrentPosition == 2)
                            {
                                item.YPosition = 0;
                                item.XPosition = item.CurrentPosition;
                            }
                            else if (item.CurrentPosition == 3 || item.CurrentPosition == 4 || item.CurrentPosition == 5)
                            {
                                item.YPosition = 1;
                                item.XPosition = item.CurrentPosition - 3;
                            }
                            item.WindownWidth = 3;
                            item.WindownHeight = 2;
                        }
                    }
                }
                foreach (Profile item in listProfile)
                {
                    if (item.IsSelected)
                    {
                        listOfTasks.Add(ResolveTaskGleamWhiteLIst(item));
                    }
                }
                //new TaskPool(int.Parse(edtThreadCount.Text),listOfTasks.ToArray());
                Tasks.StartAndWaitAllThrottled(listOfTasks, Int32.Parse(edtThreadCount.Text), Tasks.CancellationTokenSource.Token);
            }
            else
            {
                MessageBox.Show("Điền link dự án!");
            }
        }

        private void StartTaskGleam(Profile profile)
        {

            GoToWorkingURL(profile);
            Start(profile);
        }

        private void GoToWorkingURL(Profile profile)
        {
            var url = autoData?.Link;
            try
            {
                GleamDelay(5000);
                profile.Driver.Navigate().GoToUrl(url);
                /*var webDriver = profile.Driver.SwitchTo().NewWindow(WindowType.Tab);
                //profile.NetworkInterceptor.StartMonitoring();
                webDriver.Navigate().GoToUrl(url);
                //profile.NetworkInterceptor.StopMonitoring();*/
                WaitForPageLoadComplete(profile.Driver);

                CheckIfNeedToVerifyBrowser(profile.Driver);
            }
            catch (Exception e)
            {
                profile.Driver.Quit();
                MessageBox.Show(e.Message);
            }
        }

        private void AddCookie(WebDriver driver)
        {
            if (File.Exists(autoData.Cookie))
            {
                driver.Manage().Cookies.DeleteAllCookies();
                string json = File.ReadAllText(autoData.Cookie);
                if (String.IsNullOrEmpty(json))
                {
                    MessageBox.Show("File trống");
                    return;
                }
                var cookieList = JsonConvert.DeserializeObject<List<MyCookie>>(json);
                cookieList.RemoveAll(s => s.name.Equals("_app_session"));
                foreach (MyCookie myCookie in cookieList)
                {
                    // Lax", "Strict" or "None"  
                    var sameSite = "None";
                    var timeZone = TimeZoneInfo.FindSystemTimeZoneById("SE Asia Standard Time");
                    var dateTime = DateTimeOffset.FromUnixTimeSeconds(1672328273).DateTime;
                    var dateByTimeZone = TimeZoneInfo.ConvertTimeFromUtc(dateTime, timeZone);
                    OpenQA.Selenium.Cookie cookie = new OpenQA.Selenium.Cookie(myCookie.name, myCookie.value, myCookie.domain, myCookie.path, dateByTimeZone, myCookie.secure, false, sameSite);
                    driver.Manage().Cookies.AddCookie(cookie);
                }
                Trace.WriteLine("add cookie thành công");
            }
            else
            {
                MessageBox.Show("Không tìm thấy file cookie");
            }

        }

        private void CheckIfNeedToLoginFirst(Profile profile)
        {
            var tw = profile.ProfileChrome.TweeterName.Replace("@", "");
            var isLogin = GleamFindElements(profile.Driver, By.XPath("//*[@class='fab fa-twitter']//parent::*[@uib-tooltip='" + tw + "']")).Count > 0;
            var loginGmail = GleamFindElement(profile.Driver, By.XPath("//*[@class='fab fa-twitter']"));
            if (!isLogin)
            {
                if (!GleamClickButtonIfPossible(loginGmail))
                {
                    GleamTryClickButtonByScript(profile.Driver, loginGmail);
                }
                GleamDelay(1000);
                while (profile.Driver.WindowHandles.Count > 1)
                {
                    GleamDelay(1000);
                }
                profile.Driver.Navigate().GoToUrl(autoData.Link);
                WaitForPageLoadComplete(profile.Driver);
            }
        }

        private void GleamTryClickButtonByScript(WebDriver driver, WebElement element)
        {
            try
            {
                driver.ExecuteScript("arguments[0].click()", element);
            }
            catch (Exception e)
            {

            }
        }

        void Start(Profile profile)
        {
            try
            {
                CheckIfNeedToLoginFirst(profile);
                ResolveEntryMethod(profile);
            }
            catch (Exception e)
            {

            }

        }

        private void ResolveEntryMethod(Profile profile)
        {
        label_more_task:
            {
                WaitForPageLoadComplete(profile.Driver);
                SaveEmailRecievedNotification(profile);
                /*var requireLogin = GleamFindElement(profile.Driver, By.XPath(".//*[@ng-show='requiresInitialLogin()' or @ng-show='editingContestant()']"));
                if (requireLogin != null)
                {*/
                profile.Driver.Navigate().GoToUrl(autoData.Link);
                GleamDelay(1000);
                //}
                WaitForPageLoadComplete(profile.Driver);
                GleamDelay(1000);
                profile.Driver.ExecuteScript("window.scrollBy(0,document.body.scrollHeight)");
                /*profile.WindowAlert = profile.Driver.ExecuteScript("window.alert");
                profile.Driver.ExecuteScript("window.alert = function() {};");*/
                var listDataTrackSorted = new List<IWebElement>();
                var listTask = new List<String>();
                //var completeAbleTasks = WaitForElementsVisible(profile.Driver, By.XPath("//i[@ng-class='tallyIcon(entry_method)']//ancestor::div[contains(@class,'entry-method')]"));
                var completeAbleTasks = WaitForElementsVisible(profile.Driver, By.XPath("//i[@ng-class='tallyIcon(entry_method)' and ((not(@class) or @class='' and not(@class='fas fa-check')))]//ancestor::div[contains(@class,'entry-method')]"));
                if (completeAbleTasks != null)
                {
                    profile.CompletableTasks = completeAbleTasks.Count;
                }
                else
                {
                    profile.Driver.Quit();
                }
                var lockTasks = GleamFindElements(profile.Driver, By.XPath("//i[@ng-class='tallyIcon(entry_method)' and (@class='fas fa-lock')]//ancestor::div[contains(@class,'entry-method')]"));
                listDataTrackSorted.AddRange(completeAbleTasks);
                listDataTrackSorted.AddRange(lockTasks);
                var hasMoreTask = false;
                var moreTask = profile.Driver.FindElements(By.XPath("//div[@class='unlock-text user-links ng-scope']"));
                if (moreTask.Count > 0)
                {
                    hasMoreTask = moreTask[0].Displayed;
                }

                if (completeAbleTasks?.Count > 0)
                {
                    listTask.Clear();
                    foreach (WebElement element in listDataTrackSorted)
                    {
                        String task = GetCurrentTask(element);
                        listTask.Add(task);
                    }
                    for (int i = 0; i < listTask.Count; i++)
                    {
                        var rootElement = listDataTrackSorted[i] as WebElement;
                        switch (listTask[i])
                        {
                            case DataTrackEvent.FOLLOW_TWITTER:
                                ResolveDynamicEntry(profile, listTask[i], rootElement, () =>
                                {
                                    FollowTwitter(profile, rootElement);
                                });
                                break;
                            case DataTrackEvent.RETWEET_TWITTER:
                                ResolveDynamicEntry(profile, listTask[i], rootElement, () =>
                                {
                                    TwitterRetweet(profile, rootElement);
                                });
                                break;
                            case DataTrackEvent.TWEET_TWITTER:
                            case DataTrackEvent.TWEET_HASH_TAG_TWITTER:
                                ResolveDynamicEntry(profile, listTask[i], rootElement, () =>
                                {
                                    TwitterTweet(profile, rootElement);
                                });
                                break;
                            case DataTrackEvent.JOIN_DISCORD_SERVER:
                                /*ResolveDynamicEntry(profile, listTask[i], rootElement, () =>
                                {
                                    DiscordJoin(profile, rootElement);
                                });*/
                                break;
                            case DataTrackEvent.SUBSCRIBE_EMAIL:
                                ResolveDynamicEntry(profile, listTask[i], rootElement, () =>
                                {
                                    SubscribeEmail(profile, listTask[i], rootElement);
                                });
                                break;
                            default:
                                ResolveDynamicEntry(profile, listTask[i], rootElement, () => { });
                                break;
                        }
                    }
                    if ((profile.CompletedTasks == profile.ResolableTasks && profile.CompletableTasks > 0) || profile.CompletableTasks == 0)
                    {
                        UpdateStatus(profile, ProcessStatus.SUCCESS.ToString());
                    }
                    else
                    {
                        UpdateStatus(profile, ProcessStatus.FAILED.ToString());
                    }
                    //profile.Driver.Navigate().Refresh();
                    GleamDelay(1000);
                    profile.Driver.Quit();
                }
            }
        }

        private void UpdateStatus(Profile profile, String status)
        {
            profile.Status = status;
            profile.ProfileChrome.Status = profile.Status = status;

            using var dbContext = new ChromeProfileDbContext();
            dbContext.ProfileChrome.Update(profile.ProfileChrome);
            try
            {
                dbContext.SaveChanges();
            }
            catch (Exception e)
            {
                Trace.WriteLine(e.Message);
            }
        }


        private void ShowAlertDialog(WebDriver driver, string message)
        {
            driver.ExecuteScript("window.alert('" + message + "')");
        }

        private void SaveState(Profile profile, bool isSuccess)
        {
            using var dbContext = new ChromeProfileDbContext();
            profile.ProfileChrome.Status = isSuccess ? "SUCCESS" : "FAILED";
            dbContext.ProfileChrome.Update(profile.ProfileChrome);
            try
            {
                dbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ResolveDynamicEntry(Profile profile, string dataTrackEvent, WebElement rootElement, Action action)
        {
        lable_refresh:
            {
                var response = ResolveEntry(profile, dataTrackEvent, rootElement);
                if (response != null)
                {
                    var obj = System.Text.Json.JsonDocument.Parse(response);
                    if (obj.RootElement.TryGetProperty("worth", out var result))
                    {
                        profile.CompletedTasks += 1;
                    }
                    else if (obj.RootElement.TryGetProperty("error", out var error))
                    {
                        if (error.ToString().Equals("error_auth_expired"))
                        {
                            GleamClickEntryButton(profile.Driver, rootElement);
                            GleamDelay(3000);
                            while (profile.Driver.WindowHandles.Count > 1)
                            {
                                GleamDelay(1000);
                            }
                            GleamDelay(1000);
                        }
                        else if (error.ToString().Equals("not_logged_in"))
                        {
                            var responseContestant = SaveEmailRecievedNotification(profile);
                            var objContestant = System.Text.Json.JsonDocument.Parse(responseContestant);
                            if (objContestant.RootElement.TryGetProperty("contestant", out var result1))
                            {
                                goto lable_refresh;
                            }
                            else
                            {
                                profile.Driver.ExecuteScript("window.alert('not_logged_in')");
                            }
                        }
                        else if (error.ToString().Equals("error_third_party"))
                        {
                            GleamDelay(1000);
                        }
                        if (!String.IsNullOrEmpty(profile.TaskUrl))
                        {
                            action();
                            GleamDelay(1000);
                        }
                        TryAgainResolveEntry(profile, dataTrackEvent, rootElement);
                    }
                    // deprecate
                    else if (obj.RootElement.TryGetProperty("require_campaign_refresh", out var refresh1))
                    {
                        if (refresh1.ToString().Equals("True"))
                        {
                            profile.Driver.Quit();
                            /*try
                            {
                                ResolveEntryMethod(profile);
                            }
                            catch (Exception e)
                            {
                                profile.Driver.Quit();
                            }*/
                        }
                    }
                    else if (obj.RootElement.TryGetProperty("error_challenge_failed", out var challenge))
                    {
                        if (!String.IsNullOrEmpty(profile.TaskUrl))
                        {
                            action();
                            GleamDelay(1000);
                        }
                        else
                        {
                            HandleOnEntryType(profile, dataTrackEvent, rootElement);
                        }
                        if (profile.SimulatorStatus == false)
                        {
                            profile.Driver.ExecuteScript("window.alert('error_challenge_failed')");
                            var resolvedCaptcha = WaitHCaptchaResolve(profile.Driver);
                            if (resolvedCaptcha)
                            {
                                TryAgainResolveEntry(profile, dataTrackEvent, rootElement);
                            }
                        }
                    }
                    else
                    {
                        profile.Status = ProcessStatus.FAILED.ToString();
                    }
                }
            }
        }

        private bool IsRequireLogin(WebElement rootElement)
        {
            var requireLogin = GleamFindElement(rootElement, By.XPath(".//*[@name='contestantForm']"));
            if (requireLogin != null)
            {
                return true;
            }
            return false;
        }

        private void SaveLogin(Profile profile, WebElement rootElement)
        {
            var name = GleamFindElement(profile.Driver, By.XPath(".//*[@name='name']"));
            var email = GleamFindElement(profile.Driver, By.XPath(".//*[@name='email']"));
            var date_of_birth_month = GleamFindElement(profile.Driver, By.XPath(".//*[@name='date_of_birth-month']"));
            var date_of_birth_day = GleamFindElement(profile.Driver, By.XPath(".//*[@name='date_of_birth-day']"));
            var date_of_birth_year = GleamFindElement(profile.Driver, By.XPath(".//*[@name='date_of_birth-year']"));
            var btnSave = GleamFindElement(profile.Driver, By.XPath(".//*[@ng-click='setContestant()']"));
            GleamSendKeysIfPossible(profile.ProfileChrome.GmailAddress.Split("@")[0], name);
            GleamSendKeysIfPossible(profile.ProfileChrome.GmailAddress.Split("@")[0], email);
            GleamSendKeysIfPossible(profile.ProfileChrome.GmailAddress.Split("@")[0], date_of_birth_month);
            GleamSendKeysIfPossible(profile.ProfileChrome.GmailAddress.Split("@")[0], date_of_birth_day);
            GleamSendKeysIfPossible(profile.ProfileChrome.GmailAddress.Split("@")[0], date_of_birth_year);
            var clickable = GleamClickButtonIfPossible(btnSave);
            if (clickable)
            {
                profile.SimulatorStatus = true;
            }
        }

        private void SimulatorClick(Profile profile, string dataTrackEvent, WebElement rootElement)
        {
            var clickEntry = GleamClickEntryButton(profile.Driver, rootElement);
            if (clickEntry)
            {
                GleamDelay(3000);
                if (IsRequireLogin(rootElement))
                {
                    SaveLogin(profile, rootElement);
                }
                else
                {
                    HandleOnEntryType(profile, dataTrackEvent, rootElement);
                }
            }
        }

        private void HandleOnEntryType(Profile profile, string dataTrackEvent, WebElement rootElement)
        {
            switch (dataTrackEvent)
            {
                case DataTrackEvent.SUBSCRIBE_EMAIL:
                    SubscribeEmail(profile, dataTrackEvent, rootElement);
                    break;
                default:
                    DefaultAction(profile, dataTrackEvent, rootElement);
                    break;
            }
            GleamDelay(1000);
            if (IsRequireLogin(rootElement))
            {
                SaveLogin(profile, rootElement);
            }
        }

        private void GoToUrl(Profile profile, string dataTrackEvent, WebElement rootElement)
        {
            var btnUrl = GetButtonUrl(rootElement);
            var clickable = GleamClickButtonIfPossible(btnUrl);
            if (clickable)
            {
                GleamDelay(10000);
                var tab2 = SwitchToTab2(profile.Driver);
                tab2.Close();
                GleamDelay(1000);
                SwitchToMainTab(profile.Driver);
                GleamClickButtonContinue(rootElement);
                profile.SimulatorStatus = true;
            }
        }

        private void DefaultAction(Profile profile, string dataTrackEvent, WebElement rootElement)
        {
            GoToUrl(profile, dataTrackEvent, rootElement);
            FillFormCustom(profile, dataTrackEvent, rootElement);
        }
        private void FillFormCustom(Profile profile, string dataTrackEvent, WebElement rootElement)
        {
            {
                var text = "";
                var texarea = GleamFindElement(rootElement, By.XPath(".//*[ng-@class='{invalid: continueDisabled(entry_method)}']"));
                if (dataTrackEvent == DataTrackEvent.SUBMIT_URL)
                {
                    text = "https://twitter.com/home";
                }
                else
                {
                    text = profile.ProfileChrome.Wallet;
                }
                var sendKeysAble = GleamSendKeysIfPossible(text, texarea);
                if (sendKeysAble)
                {
                    GleamDelay(1000);
                    GleamClickSaveEntryButton(profile.Driver, rootElement);
                    profile.SimulatorStatus = true;
                }
            }
        }
        private void SubscribeEmail(Profile profile, string dataTrackEvent, WebElement rootElement)
        {
            GleamDelay(1000);
            var checkbox = GleamClickButtonIfPossible(GleamFindElement(rootElement, By.XPath(".//*[@class='checkbox']")));
            if (checkbox)
            {
                GleamDelay(1000);
                GleamClickSaveEntryButton(profile.Driver, rootElement);
                profile.SimulatorStatus = true;
            }
        }

        private void GetChalengeResponse()
        {
            /* HttpRequest request = new HttpRequest();
             request.AddHeader("content-type", "application/json");
             request.AddHeader("cookie", getAllCookie(driver));
             request.AddHeader("referer", profile.TaskUrl);
             request.AddHeader("user-agent", profile.ProfileChrome.UserAgent);
             request.AddHeader("authorization", token);
             request.Proxy = new HttpProxyClient(profile.IpProxy, Int32.Parse(profile.PortProxy), autoData.ProxyLoginId, autoData.ProxyLoginPassword);
             var payload = new Discord(autoData.DiscordLoginId, autoData.DiscordLoginPassword);
             var response = request.Post(profile.TaskUrl, "{}", "application/json");*/
        }

        private void TryAgainResolveEntry(Profile profile, string dataTrackEvent, WebElement rootElement)
        {
            var response = ResolveEntry(profile, dataTrackEvent, rootElement, true);
            if (response != null)
            {
                var obj = System.Text.Json.JsonDocument.Parse(response);
                if (obj.RootElement.TryGetProperty("worth", out var result))
                {
                    profile.CompletedTasks += 1;
                }
                else
                {
                    Trace.WriteLine("TryAgain : " + response);
                }
                profile.Status = ProcessStatus.FAILED.ToString();
            }
        }

        private void FollowTwitter(Profile profile, WebElement rootElement)
        {
            WebDriver driver2 = (WebDriver)profile.Driver.SwitchTo().NewWindow(WindowType.Tab);
            driver2.Navigate().GoToUrl(profile.TaskUrl);
            WaitForPageLoadComplete(driver2);
            var btnFollow = WaitForElementVisible(driver2, By.XPath("//span[text()='Chào mừng bạn!' or text()='Welcome!']/ancestor::h1/following::div[2]/descendant::span/descendant::span[text()='Follow' or text()='Theo dõi']"));
            GleamDelay(1000);
            var clickAble = GleamClickButtonIfPossible(btnFollow);
            if (clickAble)
            {
                GleamDelay(3000);
                driver2.Close();
                SwitchToMainTab(profile.Driver);
                GleamDelay(1000);
            }
        }

        private void TwitterTweet(Profile profile, WebElement rootElement)
        {
            WebDriver driver2 = (WebDriver)profile.Driver.SwitchTo().NewWindow(WindowType.Tab);
            driver2.Navigate().GoToUrl(profile.TaskUrl);
            WaitForPageLoadComplete(driver2);
            GleamDelay(2000);
            var tweetIt1 = GleamFindElement(driver2, By.XPath("//div[@aria-label='Add Tweet']//following::div[1]"));
            var tweetIt2 = GleamFindElement(driver2, By.XPath("//div[@dir='auto']//descendant::*[text() ='Tweet']"));
            var clickAble1 = GleamClickButtonIfPossible(tweetIt1);
            var clickAble2 = GleamClickButtonIfPossible(tweetIt2);
            if (clickAble1 || clickAble2)
            {
                GleamDelay(3000);
                driver2.Close();
                SwitchToMainTab(profile.Driver);
                GleamDelay(1000);
            }
        }
        private void TwitterRetweet(Profile profile, WebElement rootElement)
        {
            WebDriver driver2 = (WebDriver)profile.Driver.SwitchTo().NewWindow(WindowType.Tab);
            driver2.Navigate().GoToUrl(profile.TaskUrl);
            WaitForPageLoadComplete(driver2);
            var btnRetweet = WaitForElementVisible(driver2, By.XPath("//span[text()='Chào mừng bạn!' or text()='Welcome!']/ancestor::h1/following::div[2]/descendant::span/descendant::span[text()='Retweet']"));
            GleamDelay(1000);
            var clickAble = GleamClickButtonIfPossible(btnRetweet);
            if (clickAble)
            {
                GleamDelay(3000);
                driver2.Close();
                SwitchToMainTab(profile.Driver);
                GleamDelay(1000);
            }
        }

        private void DiscordJoin(Profile profile, WebElement rootElement)
        {
            WebDriver driver2 = (WebDriver)profile.Driver.SwitchTo().NewWindow(WindowType.Tab);
            driver2.Navigate().GoToUrl(profile.TaskUrl);
            WaitForPageLoadComplete(driver2);
            var btnkAccept = WaitForElementVisible(driver2, By.XPath("//div[contains(text(),'Chấp nhận lời mời') or contains(text(),'Accept Invite')]"));
            GleamDelay(1000);
            var clickAble = GleamClickButtonIfPossible(btnkAccept);
            if (clickAble)
            {
                GleamDelay(2000);
                WaitForPageLoadComplete(driver2);
                /*var captcha = GleamFindElement(driver2, By.XPath("//*[@class='captcha-solver']"));
                if (captcha != null)
                {
                    while (GleamFindElements(driver2, By.XPath("//*[@class='captcha-solver']")).Count > 0)
                    {
                        GleamDelay(2000);
                    }
                    //GleamClickButtonIfPossible(btnkAccept);
                    GleamDelay(1000);
                }
                WaitForPageLoadComplete(driver2);*/
                driver2.Close();
                SwitchToMainTab(profile.Driver);
                GleamDelay(1000);
            }
        }
        private string SaveEmailRecievedNotification(Profile profile)
        {
            HttpRequest request = new HttpRequest();
            request.AddHeader("content-type", "application/json;charset=UTF-8");
            request.AddHeader("cookie", getAllCookie(profile.Driver));
            request.AddHeader("Referer", linkReferer);
            request.AddHeader("user-agent", profile.ProfileChrome.UserAgent);
            request.Proxy = new HttpProxyClient(profile.IpProxy, Int32.Parse(profile.PortProxy), autoData.ProxyLoginId, autoData.ProxyLoginPassword);
            var payload = new Contestant();
            payload.campaign_key = getCampaignKey(profile);
            var contestantItem = new ContestantItem();
            contestantItem.email = profile.ProfileChrome.GmailAddress;
            contestantItem.firstname = profile.ProfileChrome.GmailAddress.Split("@")[0];
            contestantItem.lastname = GenerateName(7);
            contestantItem.name = profile.ProfileChrome.GmailAddress.Split("@")[0];
            contestantItem.competition_subscription = null;
            contestantItem.date_of_birth = "1996-12-12";
            contestantItem.stored_dob = "1996-12-12";
            contestantItem.bep20_wallet_address = profile.ProfileChrome.Wallet;
            contestantItem.twitter_account = profile.ProfileChrome.TweeterName;
            contestantItem.telegram_account = profile.ProfileChrome.TweeterName.Replace("@", "");
            contestantItem.send_confirmation = true;
            payload.contestant = contestantItem;
            payload.additional_details = true;
            try
            {
                var response = request.Post("https://gleam.io/set-contestant", JsonConvert.SerializeObject(payload), "application/json;charset=UTF-8");
                Trace.WriteLine("infor response: " + response);
                if (System.Text.Json.JsonDocument.Parse(response.ToString()).RootElement.TryGetProperty("contestant", out var value))
                {
                    profile.ContestantId = value.GetProperty("id").GetRawText();
                }
                return response.ToString();
            }
            catch (Exception e)
            {
                Trace.WriteLine(e.Message);
                return e.Message;
            }
        }

        public static string GenerateName(int len)
        {
            Random r = new Random();
            string[] consonants = { "b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "l", "n", "p", "q", "r", "s", "sh", "zh", "t", "v", "w", "x" };
            string[] vowels = { "a", "e", "i", "o", "u", "ae", "y" };
            string Name = "";
            Name += consonants[r.Next(consonants.Length)].ToUpper();
            Name += vowels[r.Next(vowels.Length)];
            int b = 2; //b tells how many times a new letter has been added. It's 2 right now because the first two letters are already in the name.
            while (b < len)
            {
                Name += consonants[r.Next(consonants.Length)];
                b++;
                Name += vowels[r.Next(vowels.Length)];
                b++;
            }
            return Name;
        }

        private string ResolveEntry(Profile profile, string dataTrackEvent, WebElement rootElement, bool retry = false)
        {
            profile.dataTrackEvent = dataTrackEvent;
            profile.entryType = GetCurrentEntryType(dataTrackEvent);
            profile.isCustomDetails = CheckIfCustomDetails(dataTrackEvent);
            profile.TaskUrl = GetUrl(rootElement);
            if (isAbleToDoThisEntry(profile, dataTrackEvent, rootElement, retry))
            {
                return MakeRequest(profile, rootElement);
            }
            return null;
        }

        private string GetUrl(WebElement rootElement)
        {
            var element = rootElement.FindElements(By.XPath(".//*[@class='expandable']//*[contains(@href,'https://') or contains(@ng-href,'https://')]"));
            if (element.Count() > 0)
            {
                return element[0].GetAttribute("href");
            }
            return null;
        }

        private WebElement GetButtonUrl(WebElement rootElement)
        {
            var element = rootElement.FindElements(By.XPath(".//*[@class='expandable']//*[contains(@href,'https://') or contains(@ng-href,'https://')]"));
            if (element.Count() > 0)
            {
                return element[0] as WebElement;
            }
            return null;
        }

        private bool isAbleToDoThisEntry(Profile profile, string dataTrackEvent, WebElement rootElement, bool retry = false)
        {
            var isOK = rootElement.FindElements(By.XPath(".//*[@class='expandable']//*[contains(@href,'https://') or contains(@ng-href,'https://')]")).Count() > 0 ||
                (dataTrackEvent != DataTrackEvent.SHARE_ACTION && dataTrackEvent != DataTrackEvent.LOYALTY && dataTrackEvent != DataTrackEvent.JOIN_DISCORD_SERVER);
            if (isOK && retry == false)
            {
                profile.ResolableTasks += 1;
            }
            return isOK;
        }

        private bool CheckIfCustomDetails(string dataTrackEvent)
        {
            return dataTrackEvent != DataTrackEvent.FOLLOW_TWITTER &&
                    dataTrackEvent != DataTrackEvent.RETWEET_TWITTER &&
                    dataTrackEvent != DataTrackEvent.JOIN_DISCORD_SERVER &&
                    dataTrackEvent != DataTrackEvent.TWEET_TWITTER &&
                    dataTrackEvent != DataTrackEvent.TWEET_HASH_TAG_TWITTER;
        }
        private string GetCurrentEntryType(string action)
        {
            var str = "";
            var arr = action.Replace("###APP_NAME### ", "").Split("|");
            if (arr.Count() > 2)
            {
                str = "'" + arr[1] + "_" + arr[2] + "'";
            }
            else
            {
                str = "'" + arr[0] + "_" + arr[1] + "'";
            }
            return str;
        }

        private string GetCurrentTask(WebElement rootElement)
        {
            return rootElement.FindElement(By.XPath(".//a[1]")).GetAttribute("data-track-event");
        }

        private string MakeRequest(Profile profile, WebElement rootElement)
        {

            HttpRequest request = new HttpRequest();
            request.Cookies = new CookieStorage();
            var cookie = getAllCookie(profile.Driver);
            AddCookie(request, cookie);
            //request.AddHeader("accept", "application/json, text/plain, */*");
            //request.AddHeader("accept-language", "vi,vi-VN;q=0.9,fr-FR;q=0.8,fr;q=0.7,en-US;q=0.6,en;q=0.5");
            request.AddHeader("content-type", "application/json;charset=UTF-8");
            request.AddHeader("cookie", cookie);
            request.AddHeader("referer", linkReferer);
            request.AddHeader("user-agent", profile.ProfileChrome.UserAgent);
            //request.AddHeader("origin", "https://gleam.io");
            //request.AddHeader("accept-encoding", "gzip, deflate, br");
            //request.AddHeader("path", "/enter/0N6xk/6073457");
            //request.AddHeader("method", "POST");
            //request.AddHeader("authority", "gleam.io");
            //request.AddHeader("scheme", "https");
            //request.AddHeader("x-csrf-token", profile.Driver.FindElement(By.XPath("//meta[@name='csrf-token']")).GetAttribute("content"));
            //request.AddHeader("x-csrf-token", HttpUtility.UrlDecode(profile.Driver.Manage().Cookies.GetCookieNamed("XSRF-TOKEN").Value));
            //AddCookie(request, cookie);
            request.Proxy = new HttpProxyClient(profile.IpProxy, Int32.Parse(profile.PortProxy), autoData.ProxyLoginId, autoData.ProxyLoginPassword);
            var postLink = "https://gleam.io/enter/" + getCampaignKey(profile) + "/" + getCurrentEntryMethodID(rootElement);
            try
            {
                var response = request.Post(postLink, getPayload(profile, rootElement), "application/json;charset=UTF-8");
                Trace.WriteLine("response : " + response);
                if (response.ToString().Contains("hash_error"))
                {
                    Trace.WriteLine(profile.ProfileChrome.ProfileName);
                    Trace.WriteLine(getPayload(profile, rootElement));
                    getPayload(profile, rootElement);
                }
                return response.ToString();
            }
            catch (Exception e)
            {
                Trace.WriteLine(e.Message);
                return null;
            }
        }

        private void AddCookie(HttpRequest request, string cookie)
        {
            var cookieArr = cookie.Split(";").ToList();
            cookieArr.ForEach(x =>
            {
                var trim = x.Split("=");
                if (trim.Count() > 1)
                {
                    request.Cookies.Add(new System.Net.Cookie(trim[0], trim[1], "/", "gleam.io"));
                }
            });
        }

        private void AddCookie(WebDriver driver, string cookie)
        {
            var cookieArr = cookie.Split(";").ToList();
            cookieArr.ForEach(x =>
            {
                var trim = x.Split("=");
                if (trim.Count() > 1)
                {
                    driver.Manage().Cookies.AddCookie(new OpenQA.Selenium.Cookie(trim[0], trim[1], "gleam.io", "/", null));
                }
            });
        }

        private string getCookie(Profile profile)
        {
            var cookie = "";
            var allCookie = profile.Driver.Manage().Cookies.AllCookies.ToList();
            allCookie.ForEach(x =>
            {
                if (x.Name.Equals("__cf_bm") || x.Name.Equals("cf_clearance") || x.Name.Equals("XSRF-TOKEN") || x.Name.Equals("_app_session"))
                    cookie += x.Name + "=" + x.Value + ((x.Name != allCookie[allCookie.Count - 1].Name) ? ";" : "");
            });
            return cookie;
        }

        private string getAllCookie(WebDriver driver)
        {
            var cookie = "";
            var allCookie = driver.Manage().Cookies.AllCookies.ToList();
            allCookie.ForEach(x =>
            {
                cookie += x.Name + "=" + x.Value + ((x.Name != allCookie[allCookie.Count - 1].Name) ? ";" : "");
            });
            return cookie;
        }

        private string getPayload(Profile profile, WebElement rootElement)
        {
            var dbg = new DBG();
            dbg.car = true;
            dbg.afd = new AFD();
            dbg.afd.afd = new Dictionary<string, Entry>();
            dbg.eds = new EDS();
            dbg.eds.eds = new Dictionary<string, Entry>();
            dbg.eds.eds.Add(getCurrentEntryMethodID(rootElement), new Entry(profile.ProfileChrome.TweeterName.Replace("@", "")));
            dbg.efd = new EFD();
            dbg.efd.efd = new Dictionary<string, Entry>();
            dbg.efd.efd.Add(getCurrentEntryMethodID(rootElement), new Entry(profile.ProfileChrome.TweeterName.Replace("@", "")));
            //dbg.efd.efd.Add("6103192", new Entry("hoangthanhLy3"));
            var dbge = new DBGE();
            dbge.eed = "2";
            dbge.hed = "#" + getCurrentEntryMethodID(rootElement) + ":undefined:undefined:undefined";
            dbge.csefr = "rfalse";
            dbge.csefn = "#undefined:false";
            dbge.ae = "re";
            dbge.aebps = "ae#2";
            dbge.re = "sed";
            var detail = new DETAIL();
            detail.twitter_username = profile.ProfileChrome.TweeterName.Replace("@", "");
            if (profile.dataTrackEvent == DataTrackEvent.JOIN_DISCORD_SERVER ||
                profile.dataTrackEvent == DataTrackEvent.RETWEET_TWITTER)
            {
                var payloadCustom = new GleamPayloadNonDetails();
                payloadCustom.dbg = dbg;
                payloadCustom.dbge = dbge;
                payloadCustom.challenge_response = null;
                payloadCustom.h = getHashHValue(profile, rootElement);
                payloadCustom.f = getHashFValue(profile);
                payloadCustom.use_hcaptcha = true;
                payloadCustom.rid = false;
                return JsonConvert.SerializeObject(payloadCustom);
            }
            else if (profile.isCustomDetails)
            {
                var payloadCustom = new GleamPayloadCustomAction();
                payloadCustom.dbg = dbg;
                payloadCustom.dbge = dbge;
                payloadCustom.challenge_response = null;
                var wallet = profile.ProfileChrome.Wallet;
                payloadCustom.details = profile.dataTrackEvent == DataTrackEvent.SUBMIT_URL ? "https://twitter.com/home" : (profile.dataTrackEvent == DataTrackEvent.SECRET_CODE ? autoData.secretCode : wallet);
                payloadCustom.h = getHashHValue(profile, rootElement);
                payloadCustom.f = getHashFValue(profile);
                payloadCustom.use_hcaptcha = true;
                payloadCustom.rid = false;
                return JsonConvert.SerializeObject(payloadCustom);
            }
            else
            {
                var gleamPayload = new GleamPayload();
                gleamPayload.dbg = dbg;
                gleamPayload.dbge = dbge;
                gleamPayload.challenge_response = null;
                gleamPayload.details = detail;
                gleamPayload.h = getHashHValue(profile, rootElement);
                gleamPayload.f = getHashFValue(profile);
                gleamPayload.use_hcaptcha = true;
                gleamPayload.rid = false;
                return JsonConvert.SerializeObject(gleamPayload);
            }
        }

        private string getCurrentEntryMethodID(WebElement rootElement)
        {
            return rootElement.GetAttribute("id").Replace("em", "").ToString();
        }

        private string getCampaignKey(Profile profile)
        {
            var campaign = profile.Driver.FindElement(By.XPath("//div[@class='campaign competition language-en ng-scope']")).GetAttribute("ng-init");
            campaign = campaign.Replace("initContestant(", "");
            campaign = Regex.Replace(campaign, "[)](; initEntryCount)[(].*[)]", "");
            var jsonObj = System.Text.Json.JsonDocument.Parse(campaign).RootElement.GetProperty("contestant");
            var key = "";
            try
            {
                key = jsonObj.GetProperty("viral_share_paths").GetRawText().Split(":")[0].Replace("{", "").ToString().Replace("\"", "");
            }
            catch (Exception e)
            {
                if (e is KeyNotFoundException)
                {
                    key = Regex.Match(autoData.Link, @"^(https:)(\/\/)(gleam.io)(\/)(.*)(\/)").Groups[5].Value;
                }
            }
            return key;
        }

        private string getHashHValue(Profile profile, WebElement rootElement)
        {
            var campaign = profile.Driver.FindElement(By.XPath("//div[@class='campaign competition language-en ng-scope']")).GetAttribute("ng-init");
            campaign = campaign.Replace("initContestant(", "");
            campaign = Regex.Replace(campaign, "[)](; initEntryCount)[(].*[)]", "");
            var jsonObj = System.Text.Json.JsonDocument.Parse(campaign).RootElement.GetProperty("contestant");
            var contestantID = "";
            try
            {
                contestantID = (-Int32.Parse(jsonObj.GetProperty("id").ToString())).ToString();
            }
            catch (Exception) { }
            if (String.IsNullOrEmpty(contestantID))
            {
                contestantID = profile.ContestantId;
            }
            var campaignKey = "'" + getCampaignKey(profile) + "'";
            var entryMethodID = "'" + getCurrentEntryMethodID(rootElement) + "'";
            var json = Properties.Resources.hash_h;
            json = json.Replace("contestant.id", contestantID).Replace("entry_method.id", entryMethodID).Replace("entry_method.entry_type", profile.entryType).Replace("campaign.key", campaignKey);
            return profile.Driver.ExecuteScript(json).ToString();
        }

        private string getHashFValue(Profile profile)
        {
            var json = Properties.Resources.hash_f.Replace("ChromeUserAgent", profile.ProfileChrome.UserAgent);
            return profile.Driver.ExecuteScript(json).ToString();
        }
        private void GleamAddToWatchList(ChromeDriver driver, WebElement rootElement)
        {
            var clickAbleEntry = GleamClickEntryButton(driver, rootElement);
            if (clickAbleEntry)
            {
                GleamDelay(3000);
                var buttonClickHere = GleamClickButtonClickHere(rootElement);
                if (buttonClickHere)
                {
                    GleamDelay(3000); // Wait for website loaded
                    WebDriver tab2 = SwitchToTab2(driver);
                    WaitForPageLoadComplete(tab2);
                    tab2.Close();
                    GleamDelay(2000);
                    SwitchToMainTab(driver);
                    GleamDelay(1000);
                    GleamSendKeyFormInputVisitIfPossible(rootElement);
                    GleamClickButtonContinue(rootElement);
                    GleamDelay(3000); // Wait for + entry if needed
                }
            }
        }

        private bool HasHCaptcha(WebDriver driver)
        {
            GleamDelay(3000);
            WaitForPageLoadComplete(driver);
            var hasHcaptcha = driver.FindElements(By.XPath("//div[@class='captcha-solver']"));
            if (hasHcaptcha.Count > 0)
            {
                hasHcaptcha[0].Click();
                return true;
            }
            return false;
        }

        private bool WaitHCaptchaResolve(WebDriver driver)
        {
            GleamDelay(3000);
            var response = WaitForElementExists(driver, By.XPath("//*[@name='h-captcha-response']"));
            GleamDelay(3000);
            var hcaptcha = GleamFindElement(driver, By.XPath("//*[@class='captcha-solver-infor']"));
            try
            {
                while (String.IsNullOrEmpty(response.Text))
                {
                    GleamDelay(2000);
                }

            }
            catch (Exception e)
            {
                // element not attached to document
                Trace.WriteLine(e.Message);
            }

            return true;
        }

        private bool IsCompleteEverithing(int? taskCount, WebDriver driver)
        {
            if (taskCount != null && taskCount > 0)
            {
                var faChecks = WaitForElementsVisible(driver, By.XPath("//i[@ng-class='tallyIcon(entry_method)' and (@class='fas fa-check')]"));
                return (faChecks != null && faChecks.Count == taskCount);
            }
            return false;

        }

        private void GleamGetBonusWhenCompleteEverything(int taskCount, ChromeDriver driver, WebElement rootElement)
        {
            var faChecks = WaitForElementsVisible(driver, By.XPath("//i[@ng-class='tallyIcon(entry_method)' and (@class='fas fa-check')]"));
            if (faChecks != null && faChecks.Count == taskCount - 1)
            {
                GleamClickEntryButton(driver, rootElement);
                GleamDelay(1000);
            }
        }

        private void GleamShareOurCampaignWithFriends(ChromeDriver driver, WebElement rootElement)
        {

        }

        private void GleamVisit(WebDriver driver, WebElement rootElement)
        {
            var clickAbleEntry = GleamClickEntryButton(driver, rootElement);
            if (clickAbleEntry)
            {
                GleamDelay(3000);
                var buttonClickHere = GleamClickButtonClickHere(rootElement);
                if (buttonClickHere)
                {
                    GleamDelay(3000); // Wait for website loaded
                    WebDriver tab2 = SwitchToTab2(driver);
                    WaitForPageLoadComplete(tab2);
                    tab2.Close();
                    GleamDelay(2000);
                    SwitchToMainTab(driver);
                    GleamDelay(1000);
                    GleamSendKeyFormInputVisitIfPossible(rootElement);
                    GleamClickButtonContinue(rootElement);
                    GleamDelay(3000); // Wait for + entry if needed
                }
            }
        }

        private WebDriver SwitchToTab2(WebDriver driver)
        {
            return (WebDriver)driver.SwitchTo().Window(driver.WindowHandles[1]);
        }

        private WebDriver SwitchToMainTab(WebDriver driver)
        {
            try
            {
                return (WebDriver)driver.SwitchTo().Window(driver.WindowHandles[0]);
            }
            catch (Exception e)
            {

            }
            return null;
        }
        private void GleamLikeFaceBook(Profile profile, WebElement rootElement)
        {
            var clickAbleEntry = GleamClickEntryButton(profile.Driver, rootElement);
            if (clickAbleEntry)
            {
                GleamDelay(3000);
                var buttonClickHere = GleamClickButtonClickHere(rootElement);
                if (buttonClickHere)
                {
                    GleamDelay(2000); // Wait for website loaded
                    WebDriver tab2 = (WebDriver)profile.Driver.SwitchTo().Window(profile.Driver.WindowHandles[1]);
                    WaitForPageLoadComplete(tab2);
                    GleamDelay(3000);
                    var clickLike = GleamClickButtonIfPossible(WaitForElementVisible(tab2, By.XPath("//div[@aria-label='Thích' or @aria-label='Like']")));
                    if (clickLike)
                    {
                        GleamDelay(3000);
                        tab2.Close();
                        GleamDelay(2000);
                        profile.Driver.SwitchTo().Window(profile.Driver.WindowHandles[0]); // switch to main tab 
                        GleamClickButtonContinue(rootElement);
                        GleamDelay(2000);
                    }
                }
            }
        }

        private void GleamReTweetAndTagFriendsTwitter(Profile profile, WebElement rootElement)
        {
            var clickAbleEntry = GleamClickEntryButton(profile.Driver, rootElement);
            if (clickAbleEntry)
            {
                GleamDelay(3000);
                var buttonClickHere = GleamClickButtonClickHere(rootElement);
                if (buttonClickHere)
                {
                    GleamDelay(2000); // Wait for website loaded
                    WebDriver tab2 = (WebDriver)profile.Driver.SwitchTo().Window(profile.Driver.WindowHandles[1]);
                    WaitForPageLoadComplete(tab2);
                    GleamDelay(3000);

                    var clickToRep = GleamClickButtonIfPossible(WaitForElementVisible(tab2, By.XPath("//div[@class='DraftEditor-root']")));
                    if (clickToRep)
                    {
                        var editReply = WaitForElementExists(tab2, By.XPath("//div[@class='DraftEditor-editorContainer']//descendant::div[1]//descendant::span"));
                        GleamSendKeysIfPossible(GetRandom3FriendOnTwitter(profile), editReply);
                        var btnReply = WaitForElementVisible(tab2, By.XPath("//div[@role='button' and @data-testid='tweetButtonInline']"));
                        GleamClickButtonContinue(btnReply);
                        GleamDelay(3000);
                        tab2.Close();
                        GleamDelay(2000);
                        profile.Driver.SwitchTo().Window(profile.Driver.WindowHandles[0]); // switch to main tab 
                        GleamClickButtonContinue(rootElement);
                        GleamDelay(2000);
                    }
                }
            }
        }

        private bool GleamClickButtonClickHere(WebElement rootElement)
        {
            //return GleamClickButtonIfPossible(WaitForChildElementVisible(rootElement, By.XPath(".//div[@class='expandable']//descendant::div[contains(@class,'form-compact__part')]//child::*[last()]//descendant::*")));
            return GleamClickButtonIfPossible(WaitForChildElementVisible(rootElement, By.XPath(".//*[@class='btn btn-info btn-large btn-embossed ng-binding']")));
        }

        private void GleamTweetOnTwitter(WebDriver driver, WebElement rootElement)
        {
            var clickAbleEntry = GleamClickEntryButton(driver, rootElement);
            if (clickAbleEntry)
            {
                GleamDelay(3000);
                var buttonTweet = WaitForChildElementVisible(rootElement, By.XPath(".//div[@class='expandable']//descendant::div[last()]//descendant::*[last()]"));
                if (GleamClickButtonIfPossible(buttonTweet))
                {
                    GleamDelay(2000); // Wait for website loaded
                    WebDriver tab2 = (WebDriver)driver.SwitchTo().Window(driver.WindowHandles[1]);
                    WaitForPageLoadComplete(tab2);
                    GleamDelay(3000);
                    var tweetIt = WaitForElementVisible(tab2, By.XPath("//div[@aria-label='Add Tweet']//following::div[1]"));
                    GleamClickButtonIfPossible(tweetIt);
                    GleamDelay(3000);
                    tab2.Close();
                    GleamDelay(2000);
                    driver.SwitchTo().Window(driver.WindowHandles[0]); // switch to main tab 
                    GleamDelay(1000);
                    GleamClickButtonContinue(rootElement);
                    GleamDelay(1000);
                }
            }

        }

        private void GleamVisitWebsite(WebDriver driver, WebElement rootElement)
        {
            var clickAbleEntry = GleamClickEntryButton(driver, rootElement);
            if (clickAbleEntry)
            {
                GleamDelay(3000);
                var buttonClickHere = WaitForChildElementVisible(rootElement, By.XPath(".//div[@class='expandable']//descendant::div[@class='form-compact__content']//div[last()]//descendant::*"));
                var inputFullName = WaitForChildElementVisible(rootElement, By.XPath(".//input[@name='name']"));
                var inputEmail = WaitForChildElementVisible(rootElement, By.XPath(".//input[@name='email']"));
                if (buttonClickHere != null)
                {
                    GleamVisitWebsiteForm1(driver, buttonClickHere, rootElement);
                }
                else if (inputFullName != null && inputEmail != null)
                {
                    GleamVisitWebsiteForm2(driver, inputFullName, inputEmail, rootElement);
                }
            }

        }

        private void GleamVisitWebsiteForm1(WebDriver driver, WebElement buttonClickHere, WebElement rootElement)
        {
            var clickable = GleamClickButtonIfPossible(buttonClickHere);
            if (clickable)
            {
                GleamDelay(5000); // Wait for website loaded
                WebDriver tab2 = (WebDriver)driver.SwitchTo().Window(driver.WindowHandles[1]);
                WaitForPageLoadComplete(tab2);
                tab2.Close();
                GleamDelay(2000);
                driver.SwitchTo().Window(driver.WindowHandles[0]); // switch to main tab 
                GleamDelay(1000);
                var buttonContinue1 = WaitForChildElementVisible(rootElement, By.XPath(".//div[@class='form-actions center']//descendant::a[@class='btn btn-primary']"));
                var buttonContinue2 = WaitForChildElementVisible(rootElement, By.XPath(".//div[@class='form-actions center']//descendant:://button[@class='btn btn-primary']"));
                if (!GleamClickButtonIfPossible(buttonContinue1))
                {
                    GleamClickButtonIfPossible(buttonContinue2);
                }
                GleamDelay(3000); // Wait for + entry if needed
            }
        }
        private void GleamVisitWebsiteForm2(WebDriver driver, WebElement inputFullName, WebElement inputEmail, WebElement rootElement)
        {
            inputFullName.Clear();
            inputFullName.SendKeys("Hoang Thanh");
            inputEmail.Clear();
            inputEmail.SendKeys("hoangthanh4686385@gmail.com");
            var btnSave = WaitForChildElementVisible(rootElement, By.XPath(".//button[@class='btn btn-primary ng-scope']"));
            btnSave.Click();
        }

        private void GleamFollowTwitter(WebDriver driver, WebElement rootElement)
        {
            var clickAbleEntry = GleamClickEntryButton(driver, rootElement);
            if (clickAbleEntry)
            {
                GleamDelay(3000);
                var buttonTwitter = WaitForChildElementVisible(rootElement, By.XPath(".//span[@class='twitter-label' and normalize-space(text()) = 'Follow']"));
                var clickable = GleamClickButtonIfPossible(buttonTwitter);
                if (clickable)
                {
                    GleamDelay(2000); // Wait for website loaded
                    WebDriver tab2 = (WebDriver)driver.SwitchTo().Window(driver.WindowHandles[1]);
                    WaitForPageLoadComplete(tab2);
                    GleamDelay(3000);
                    var buttonLogin = GetElementIfExist(driver, By.XPath("//div[@data-testid='IntentLoginSheet_Login_Sheet']/div[2]/div[2]/descendant::span[last()]"));
                    if (buttonLogin != null)
                    {
                        buttonLogin.Click();
                    }
                    var buttonFollow = WaitForElementVisible(tab2, By.XPath("//span[text()='Chào mừng bạn!' or text()='Welcome!']/ancestor::h1/following::div[2]/descendant::span/descendant::span[text()='Follow' or text()='Theo dõi']"));
                    buttonFollow.Click();
                    GleamDelay(5000);
                    tab2.Close();
                    GleamDelay(2000);
                    driver.SwitchTo().Window(driver.WindowHandles[0]); // switch to main tab 
                    GleamDelay(1000);
                    var inputMyName = WaitForChildElementVisible(rootElement, By.XPath(".//input[@placeholder='@MyName']"));
                    if (inputMyName != null)
                    {
                        GleamSendKeysIfPossible("@hoangthanhLy3", inputMyName);
                        GleamClickButtonContinue(rootElement);
                    }
                    else
                    {
                        GleamClickButtonContinue(rootElement);
                    }
                    GleamDelay(1000); // Wait for + entry if needed
                }
            }
        }

        private void GleamJoinTelegramGroup(WebDriver driver, WebElement rootElement)
        {
            var clickAbleEntry = GleamClickEntryButton(driver, rootElement);
            if (clickAbleEntry)
            {
                var buttonJoinTelegram = GleamClickButtonClickHere(rootElement);
                if (buttonJoinTelegram)
                {
                    GleamDelay(2000); // Wait for website loaded
                    WebDriver tab2 = (WebDriver)driver.SwitchTo().Window(driver.WindowHandles[1]);
                    WaitForPageLoadComplete(tab2);
                    var buttonOpenInWeb = WaitForElementVisible(tab2, By.XPath("//div[@class='tgme_page_action tgme_page_web_action']"));
                    if (buttonOpenInWeb != null)
                    {
                        buttonOpenInWeb.Click();
                        GleamDelay(3000);
                    }
                    WaitForPageLoadComplete(tab2);
                    var buttonJoinGroup = GleamFindElement(tab2, By.XPath("//div[@class='HeaderActions']//button"));
                    GleamClickButtonIfPossible(buttonJoinGroup);
                    GleamDelay(1000);
                    tab2.Close();
                    GleamDelay(2000);
                    driver.SwitchTo().Window(driver.WindowHandles[0]); // switch to main tab 
                    GleamClickButtonContinue(rootElement);
                    GleamDelay(1000); // Wait for + entry if needed
                }
            }
        }

        private void GleamClickButtonContinue(WebElement rootElement)
        {
            var buttonContinue1 = WaitForChildElementVisible(rootElement, By.XPath(".//div[@class='form-actions center']//descendant::*"));
            GleamClickButtonIfPossible(buttonContinue1);
        }

        private void GleamVisitInstagram(WebDriver driver, WebElement rootElement)
        {
            var clickAbleEntry = GleamClickEntryButton(driver, rootElement);
            if (clickAbleEntry)
            {
                GleamDelay(3000);
                var buttonClickHere = GleamClickButtonClickHere(rootElement);
                if (buttonClickHere)
                {
                    GleamDelay(3000); // Wait for website loaded
                    WebDriver tab2 = (WebDriver)driver.SwitchTo().Window(driver.WindowHandles[1]);
                    WaitForPageLoadComplete(tab2);
                    tab2.Close();
                    GleamDelay(2000);
                    driver.SwitchTo().Window(driver.WindowHandles[0]); // switch to main tab 
                    GleamDelay(1000);
                    GleamSendKeyFormInputVisitIfPossible(rootElement);
                    GleamClickButtonContinue(rootElement);
                    GleamDelay(3000); // Wait for + entry if needed
                }
            }
        }

        private void GleamSendKeyFormInputVisitIfPossible(WebElement rootElement)
        {
            var inputForm = WaitForChildElementVisible(rootElement, By.XPath("//div[@class='text input optional form-group']//descendant::textarea"));
            if (inputForm != null)
            {
                inputForm.SendKeys("2022");
                GleamDelay(1000);
            }
        }

        private void GleamVisitYouTubeChannel(WebDriver driver, WebElement rootElement)
        {
            var clickAbleEntry = GleamClickEntryButton(driver, rootElement);
            if (clickAbleEntry)
            {
                GleamDelay(3000);
                var buttonClickHere = GleamClickButtonClickHere(rootElement);
                if (buttonClickHere)
                {
                    GleamDelay(3000); // Wait for website loaded
                    WebDriver tab2 = (WebDriver)driver.SwitchTo().Window(driver.WindowHandles[1]);
                    WaitForPageLoadComplete(tab2);
                    tab2.Close();
                    GleamDelay(2000);
                    driver.SwitchTo().Window(driver.WindowHandles[0]); // switch to main tab 
                    GleamDelay(1000);
                    GleamSendKeyFormInputVisitIfPossible(rootElement);
                    GleamClickButtonContinue(rootElement);
                    GleamDelay(3000); // Wait for + entry if needed
                }
            }
        }

        private void GleamInputBNBWalletBSC(WebDriver driver, WebElement rootElement)
        {
            var clickAbleEntry = GleamClickEntryButton(driver, rootElement);
            if (clickAbleEntry)
            {
                GleamDelay(3000);
                var textArea = WaitForChildElementVisible(rootElement, By.XPath(".//div[@class='expandable']//following::textarea"));
                textArea.SendKeys("0xDAb846d9d739cC6831287bCA728449df8332574E");
                GleamDelay(5000); // Wait for website loaded
                var buttonContinue1 = WaitForChildElementVisible(rootElement, By.XPath(".//a[@class='btn btn-primary']"));
                var buttonContinue2 = WaitForChildElementVisible(rootElement, By.XPath(".//button[@class='btn btn-primary']"));
                if (!GleamClickButtonIfPossible(buttonContinue1))
                {
                    GleamClickButtonIfPossible(buttonContinue2);
                }
                GleamDelay(3000); // Wait for + entry if needed
            }
        }

        private void GleamSubmitBSCWalletAddress(WebDriver driver, WebElement rootElement)
        {
            var clickAbleEntry = GleamClickEntryButton(driver, rootElement);
            if (clickAbleEntry)
            {
                GleamDelay(3000);
                var input = WaitForChildElementVisible(rootElement, By.XPath(".//div[@class='expandable']//following::input"));
                var sendKeyAble = GleamSendKeysIfPossible("0xDAb846d9d739cC6831287bCA728449df8332574E", input);
                if (sendKeyAble)
                {
                    GleamDelay(2000); // Wait for website loaded
                    var buttonContinue1 = WaitForChildElementVisible(rootElement, By.XPath(".//a[@class='btn btn-primary']"));
                    var buttonContinue2 = WaitForChildElementVisible(rootElement, By.XPath(".//button[@class='btn btn-primary']"));
                    if (!GleamClickButtonIfPossible(buttonContinue1))
                    {
                        GleamClickButtonIfPossible(buttonContinue2);
                    }
                    GleamDelay(3000); // Wait for + entry if needed
                }
            }
        }

        private void GleamJoinDiscordServer(WebDriver driver, WebElement rootElement)
        {
            var clickAbleEntry = GleamClickEntryButton(driver, rootElement);
            if (clickAbleEntry)
            {
                GleamDelay(2000);
                var buttonClickHere = WaitForChildElementVisible(rootElement, By.XPath(".//span[@class='fab fa-discord']"));
                var clickable = GleamClickButtonIfPossible(buttonClickHere);
                if (clickable)
                {
                    GleamDelay(1000); // Wait for website loaded
                    var tab2 = driver.SwitchTo().Window(driver.WindowHandles[1]) as WebDriver;
                    WaitForPageLoadComplete(tab2);
                    var buttonAceptInvite = WaitForElementVisible(tab2, By.XPath("//div[contains(text(),'Chấp nhận lời mời') or contains(text(),'Accept Invite')]"));
                    GleamClickButtonIfPossible(buttonAceptInvite);
                    GleamDelay(3000);
                    WaitForPageLoadComplete(tab2);
                    tab2.Close();
                    GleamDelay(2000);
                    driver.SwitchTo().Window(driver.WindowHandles[0]); // switch to main tab 
                    GleamDelay(1000);
                    GleamClickButtonContinue(rootElement);
                    GleamDelay(2000);
                    var tabCount = driver.WindowHandles.Count;
                    if (tabCount > 1)
                    {
                        var tabPermissionDiscord = driver.SwitchTo().Window(driver.WindowHandles[1]) as WebDriver;
                        var buttonAccept = WaitForElementVisible(tabPermissionDiscord, By.XPath("//div[contains(text(),'Phê duyệt') or contains(text(),'Authorize')]"));
                        var clickableAccept = GleamClickButtonIfPossible(buttonAccept);
                        if (clickableAccept)
                        {
                            GleamDelay(2000);
                            driver.SwitchTo().Window(driver.WindowHandles[0]); // switch to main tab 
                        }
                    }
                    GleamDelay(3000); // Wait for + entry if needed
                }
            }
        }

        private void GleamVisitFacebook(WebDriver driver, WebElement rootElement)
        {
            var clickAbleEntry = GleamClickEntryButton(driver, rootElement);
            if (clickAbleEntry)
            {
                GleamDelay(3000);
                var buttonClickHere = WaitForChildElementVisible(rootElement, By.XPath(".//a[@class='btn btn-info btn-large btn-embossed ng-binding']"));
                var clickable = GleamClickButtonIfPossible(buttonClickHere);
                if (clickable)
                {
                    GleamDelay(5000); // Wait for website loaded
                    var tab2 = driver.SwitchTo().Window(driver.WindowHandles[1]) as WebDriver;
                    WaitForPageLoadComplete(tab2);
                    GleamDelay(2000);
                    tab2.Close();
                    GleamDelay(2000);
                    driver.SwitchTo().Window(driver.WindowHandles[0]); // switch to main tab 
                    GleamDelay(1000);
                    var buttonContinue = WaitForChildElementVisible(rootElement, By.XPath(".//a[@class='btn btn-primary']"));
                    GleamClickButtonIfPossible(buttonContinue);
                    GleamDelay(3000); // Wait for + entry if needed
                }
            }
        }

        private void GleamRetweetOnTwitter(WebDriver driver, WebElement rootElement)
        {
            var clickAbleEntry = GleamClickEntryButton(driver, rootElement);
            if (clickAbleEntry)
            {
                GleamDelay(5000);
                var buttonRetweet = WaitForChildElementVisible(rootElement, By.XPath(".//div[@class='expandable']//following::a[@class='xl twitter-button']"));
                var clickable = GleamClickButtonIfPossible(buttonRetweet);
                if (clickable)
                {
                    WebDriver tab2 = (WebDriver)driver.SwitchTo().Window(driver.WindowHandles[1]);
                    WaitForPageLoadComplete(tab2);
                    GleamDelay(3000);
                    WaitForPageLoadComplete(tab2);
                    var retweet = WaitForElementVisible(tab2, By.XPath("//span[text()='Chào mừng bạn!' or text()='Welcome!']/ancestor::h1/following::div[2]/descendant::span/descendant::span[text()='Retweet']"));
                    retweet.Click();
                    GleamDelay(3000);
                    tab2.Close();
                    GleamDelay(2000);
                    driver.SwitchTo().Window(driver.WindowHandles[0]); // switch to main tab 
                    GleamDelay(1000);
                    GleamClickButtonContinue(rootElement);
                    GleamDelay(3000); // Wait for + entry if needed
                }
            }
        }

        private void GleamReferFriendsForExtraEntries(WebDriver driver, WebElement rootElement)
        {
            var clickAbleEntry = GleamClickEntryButton(driver, rootElement);
            if (clickAbleEntry)
            {
                GleamDelay(3000);
                var buttonClickHere = WaitForChildElementVisible(rootElement, By.XPath(".//i[@class='fab fa-twitter']"));
                var clickable = GleamClickButtonIfPossible(buttonClickHere);
                if (clickable)
                {
                    GleamDelay(5000); // Wait for website loaded
                    var tab2 = driver.SwitchTo().Window(driver.WindowHandles[1]) as WebDriver;
                    WaitForPageLoadComplete(tab2);
                    var btnTweet = WaitForElementVisible(driver, By.XPath("//span[text()='Tweet']"));
                    btnTweet.Click();
                    GleamDelay(2000);
                    tab2.Close();
                    GleamDelay(2000);
                    driver.SwitchTo().Window(driver.WindowHandles[0]); // switch to main tab 
                    GleamDelay(1000);
                }
            }
        }

        private void GleamFollowUsOnTweeter(WebDriver driver, WebElement rootElement)
        {
            var clickAbleEntry = GleamClickEntryButton(driver, rootElement);
            if (clickAbleEntry)
            {
                GleamDelay(3000);
            }
        }

        private TaskType GetTaskByRegex(WebElement rootElement)
        {
            var str = "";
            try
            {
                var element = rootElement.FindElement(By.XPath(".//span[@class='text user-links entry-method-title ng-scope']"));
                str = element.Text;
            }
            catch (NoSuchElementException e)
            {
                // find another xpath
                try
                {
                    var element = rootElement.FindElement(By.XPath(".//span[@class='text user-links entry-method-title ng-scope ng-binding']"));
                    str = element.Text;
                }
                catch (NoSuchElementException)
                {

                }
            }
            TaskType taskType = TaskType.UNKNOW;
            if (!String.IsNullOrEmpty(str))
            {
                var listRegex = ClassUtils.GetFieldValues(new TaskRegex());
                foreach (var regex in listRegex)
                {
                    var isMatch = Regex.IsMatch(str, regex.Value);
                    if (isMatch)
                    {
                        taskType = (TaskType)Enum.Parse(typeof(TaskType), regex.Key, true);
                        break;
                    }
                }
            }
            return taskType;
        }
        private bool GleamClickEntryButton(WebDriver driver, WebElement rootElement)
        {
            var buttonEntry = WaitForChildElementVisible(rootElement, By.XPath(".//span[@class='tally']"));
            return GleamClickButtonIfPossible(buttonEntry);
        }

        private bool GleamClickSaveEntryButton(WebDriver driver, WebElement rootElement)
        {
            var buttonEntry = WaitForChildElementVisible(rootElement, By.XPath(".//*[@ng-click='saveEntryDetails(entry_method)']"));
            return GleamClickButtonIfPossible(buttonEntry);
        }

        private bool GleamSendKeysIfPossible(string keys, WebElement element)
        {
            try
            {
                if (element != null)
                {
                    element.SendKeys("");
                    element.SendKeys(keys);
                }

            }
            catch (Exception e)
            {

            }
            return false;
        }

        private WebElement GleamFindElement(WebDriver driver, By byWhat)
        {
            WebElement element = null;
            try
            {
                element = driver.FindElement(byWhat) as WebElement;
            }
            catch (Exception e)
            {

            }
            return element;
        }

        private WebElement GleamFindElement(WebElement rootElement, By byWhat)
        {
            WebElement element = null;
            try
            {
                element = rootElement.FindElement(byWhat) as WebElement;
            }
            catch (Exception e)
            {

            }
            return element;
        }

        private List<IWebElement> GleamFindElements(WebDriver driver, By byWhat)
        {
            List<IWebElement> elements = new List<IWebElement>();
            try
            {
                elements = driver.FindElements(byWhat).ToList();
            }
            catch (Exception e)
            {
                Trace.WriteLine(e.Message);
            }
            return elements;
        }

        private bool GleamClickButtonIfPossible(WebElement element)
        {
            try
            {
                if (element != null)
                {
                    element.Click();
                    return true;
                }
            }
            catch (Exception e)
            {
                // not click able or not interacable or task is complete
                Trace.WriteLine(e.Message);
            }
            return false;
        }

        private void SetupExtensionsNessesaryIfNeeded(ChromeOptions options)
        {
            var webRtcPath = edtChromePath.Text + "\\Data\\WebRTC_Control.crx";
            var isWebRTCExist = File.Exists(webRtcPath);
            if (!isWebRTCExist)
            {
                byte[] bytes = Properties.Resources.WebRTC_Control;
                File.WriteAllBytes(webRtcPath, bytes);
                options.AddExtension(webRtcPath);
            }
            var captchaPath = edtChromePath.Text + "\\Data\\TowCaptcha.crx";
            var isCaptchaExist = File.Exists(captchaPath);
            if (!isCaptchaExist)
            {
                byte[] bytes = Properties.Resources._2Captcha_Solver;
                File.WriteAllBytes(captchaPath, bytes);
                options.AddExtension(captchaPath);
            }
            var proxyOmegaPath = edtChromePath.Text + "\\Data\\ProxyOmega.crx";
            var isProxyOmegaExist = File.Exists(proxyOmegaPath);
            if (!isCaptchaExist)
            {
                byte[] bytes = Properties.Resources.Proxy_SwitchyOmega;
                File.WriteAllBytes(proxyOmegaPath, bytes);
                options.AddExtension(proxyOmegaPath);
            }
        }

        private Task ResolveTaskGleamWhiteLIst(Profile item)
        {
            Action action = async () =>
           {
               ChromeDriverService service = ChromeDriverService.CreateDefaultService();
               service.HideCommandPromptWindow = true;
               service.SuppressInitialDiagnosticInformation = true;
               var options = new ChromeOptions();
               //options.AcceptInsecureCertificates = true;
               //options.AddAdditionalOption(CapabilityType.AcceptSslCertificates, true);

               options.BinaryLocation = item.AbsoluteProfilePath + "\\App\\Chrome-bin\\chrome.exe";
               options.AddArgument("--user-data-dir=" + item.AbsoluteProfilePath + "\\Data\\Profile");
               options.AddArgument("--profile-directory=Default");
               options.AddArgument("user-agent=" + item.ProfileChrome.UserAgent);
               //options.AddArgument("--disable-infobars");
               //options.AddArgument("--incognito");
               //options.AddArgument("--silent");
               //options.AddArgument("--no-sandbox");
               //options.AddArgument("--disable-web-security");
               //options.AddArgument("--allow-running-insecure-content");
               //options.AddArgument("--disable-blink-features");
               //options.AddArgument("--disable-blink-features=AutomationControlled");
               //options.AddExcludedArguments(new string[] { "enable-automation" });
               options.AddExcludedArguments(new string[] { "--test-type",
                   "--enable-automation",
               "--enable-blink-features",
               "--disable-blink-features",
               "--enable-logging",
               "--disable-background-networking",
               "--disable-backgrounding-occluded-windows",
               "--disable-client-side-phishing-detection",
               "--disable-hang-monitor",
               "--no-service-autorun",
               "--use-mock-keychain",
               "--allow-pre-commit-input",
               "--disable-default-apps",
               "--disable-popup-blocking",
               "--disable-prompt-on-repost",
               "--disable-sync",
               "--password-store"});
               options.AddExcludedArgument("--test-type");
               options.AddExcludedArgument("--enable-automation");
               options.AddExcludedArgument("--enable-blink-features");
               options.AddExcludedArgument("--enable-logging");
               options.AddExcludedArgument("--disable-background-networking");
               options.AddExcludedArgument("--disable-backgrounding-occluded-windows");
               options.AddExcludedArgument("--disable-client-side-phishing-detection");
               options.AddExcludedArgument("--disable-hang-monitor");
               options.AddExcludedArgument("--no-service-autorun");
               options.AddExcludedArgument("--use-mock-keychain");
               options.AddExcludedArgument("--allow-pre-commit-input");
               options.AddExcludedArgument("--disable-default-apps");
               options.AddExcludedArgument("--disable-prompt-on-repost");
               options.AddExcludedArgument("--disable-sync");
               options.AddExcludedArgument("--disable-blink-features");
               options.AddExcludedArgument("--password-store");
               options.AddAdditionalOption("useAutomationExtension", false);

               //options.AddArgument("--flag-switches-begin");
               //options.AddArgument("--flag-switches-end");
               options.AddArgument("--no-default-browser-check");
               //options.AddExcludedArgument("--test-name");
               //options.AddExcludedArgument("--testing");
               //options.AddArguments("--disable-javascript");
               //options.AddExcludedArgument("--enable-blink-features");
               //options.AddExcludedArgument("--enable-blink-test-features");
               //options.AddAdditionalOption("--webrtc.nonproxied_udp_enabled", true);
               //options.AddAdditionalOption("--enable-webrtc-stun-origin", false);
               //options.AddAdditionalOption("--force-webrtc-ip-handling-policy", "default_public_interface_only");
               //options.UnhandledPromptBehavior = UnhandledPromptBehavior.Ignore;
               //options.PageLoadStrategy = PageLoadStrategy.Eager;
               //options.AddAdditionalOption("--force-webrtc-ip-handling-policy","default_public_interface_only");
               //options.AddAdditionalOption("--disable-features", "WebRtcHideLocalIpsWithMdns");
               //options.AddUserProfilePreference("prefs", "enforce-webrtc-ip-permission-check\": True");
               //options.AddUserProfilePreference("prefs", "webrtc.nonproxied_udp_enabled\": True");
               //var proxy = new Proxy();
               //proxy.Kind = ProxyKind.AutoDetect;
               //proxy.SslProxy = (item.FullProxy);
               /*proxy.FtpProxy = (item.FullProxy);
               proxy.HttpProxy = (item.FullProxy);
               proxy.SocksProxy = (item.FullProxy);
               proxy.SocksVersion = 5;
               proxy.SocksUserName = autoData.ProxyLoginId;
               proxy.SocksPassword = autoData.ProxyLoginPassword;*/
               //options.Proxy = proxy;
               try
               {
                   //ChromeDriver driver = new ChromeDriver(service, options);
                   ChromeDriver driver = new ChromeDriver("E:\\undetected driver chrome\\");
                   /*NetworkAuthenticationHandler handler = new NetworkAuthenticationHandler()
                   {
                       UriMatcher = d => true, //d.Host.Contains("your-host.com")
                       Credentials = new PasswordCredentials(autoData.ProxyLoginId, autoData.ProxyLoginPassword)
                   };
                   var networkInterceptor = driver.Manage().Network;
                   networkInterceptor.AddAuthenticationHandler(handler);*/
                   item.Driver = driver;
                   item.DriverService = service;
                   //item.NetworkInterceptor = networkInterceptor;
               }
               catch (Exception e)
               {
                   MessageBox.Show(e.Message);
               }
               if (item.Driver != null)
               {
                   if (threadCount <= 6)
                   {
                       if (autoData.windowSize.IsEmpty)
                       {
                           item.Driver.Manage().Window.Maximize();
                           GleamDelay(1000);
                           autoData.windowSize = item.Driver.Manage().Window.Size;
                       }
                       int desiredHeight = autoData.windowSize.Height / item.WindownHeight;
                       int desiredWidth = autoData.windowSize.Width / item.WindownWidth;
                       System.Drawing.Size desiredSize = new System.Drawing.Size(desiredWidth, desiredHeight);
                       item.Driver.Manage().Window.Size = desiredSize;
                       item.Driver.Manage().Window.Position = new System.Drawing.Point(desiredWidth * item.XPosition, desiredHeight * item.YPosition);
                   }
                   StartTaskGleam(item);
               }
           };
            Task task = new Task(action, Tasks.CancellationTokenSource.Token);
            return task;
        }

        private void CreateNetWorkCredental()
        {
            networkAuthenticationHandler = new NetworkAuthenticationHandler()
            {
                UriMatcher = d => true, //d.Host.Contains("your-host.com")
                Credentials = new PasswordCredentials(autoData.ProxyLoginId, autoData.ProxyLoginPassword)
            };
        }

        private string GetRandomUserAgent()
        {
            Random r = new Random();
            var listUserAgent = File.ReadAllLines("D:\\Crypto\\user-agents_win10.txt");
            int index = r.Next(0, listUserAgent.Count());
            return listUserAgent[index];
        }

        private void edtCount_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                var num = Int32.Parse((sender as TextBox).Text) + 1;
                for (int i = 0; i < listProfile.Count; i++)
                {
                    var status = listProfile[i].Status == ProcessStatus.REST.ToString() || listProfile[i].Status == ProcessStatus.FAILED.ToString();
                    if (i <= num && status)
                    {
                        listProfile[i].IsSelected = true;
                    }
                    else
                    {
                        listProfile[i].IsSelected = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void CheckBoxChanged(object sender, RoutedEventArgs e)
        {
            var isChecked = (sender as CheckBox).IsChecked ?? false;
            foreach (Profile item in listProfile)
            {
                if (isChecked)
                {
                    var status = item.Status == ProcessStatus.REST.ToString() || item.Status == ProcessStatus.FAILED.ToString();
                    if (status)
                    {
                        item.IsSelected = true;
                    }
                    else
                    {
                        item.IsSelected = false;
                    }
                }
                else
                {
                    item.IsSelected = (sender as CheckBox).IsChecked ?? false;
                }

            }
            var countSelected = listProfile.Where(p => p.IsSelected).Count();
            //edtProfileCount.Text = countSelected.ToString();
        }

        private void RunChromeProfileCommand()
        {
            List<string> cmds = new List<string>();
            for (int i = 1; i < 101; i++)
            {
                var str = "\"C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe\" --profile-directory=\"Profile " + i + "\"";
                cmds.Add(str);
                Trace.WriteLine(str);
                Task.Delay(1000);
            }
            RunCommands(cmds, "C:\\Users\\hoanm\\AppData\\Local\\Google\\Chrome");
        }

        private void CreateNewChromeProfile()
        {
            for (int i = 1; i < 101; i++)
            {
                var str = "\"C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe\" --profile-directory=\"Profile " + i + "\"";
                Trace.WriteLine(str);
                Task.Delay(1000);
                ExecuteCommand(str);
            }

        }
        private void FillProxyExtension(WebDriver driver, Profile profile)
        {
            Actions build = new Actions(driver);
            var proxyLink = "chrome-extension://padekgcemlokbadohgkifijomclgjgif/options.html#!/profile/proxy";
            driver.Navigate().GoToUrl(proxyLink);
            driver.SwitchTo().ActiveElement();
            WebElement inputProxy = WaitForElementVisible(driver, By.XPath("//tbody/*/td/input[@ng-model='proxyEditors[scheme].host']"));
            WebElement inputPort = WaitForElementVisible(driver, By.XPath("//tbody/*/td/input[@ng-model='proxyEditors[scheme].port']"));
            WebElement lockProxy = WaitForElementVisible(driver, By.XPath("//button[@ng-click='editProxyAuth(scheme)']"));
            inputProxy.Clear();
            inputProxy.SendKeys(profile.IpProxy);
            inputPort.Clear();
            inputPort.SendKeys(profile.PortProxy);
            build.MoveToElement(lockProxy).Build().Perform();
            lockProxy.Click();
            GleamDelay(1000);
            WebElement userName = WaitForElementVisible(driver, By.XPath("//input[@placeholder='Username']"));
            userName.Clear();
            userName.SendKeys("hoanm1206");
            WebElement seePassWord = WaitForElementVisible(driver, By.XPath("//button[@title='Show password']"));
            seePassWord.Click();
            WebElement passWord = WaitForElementVisible(driver, By.XPath("//input[@placeholder='Password']"));
            passWord.Clear();
            passWord.SendKeys("830794800");
            WebElement saveChange = WaitForElementVisible(driver, By.XPath("//button[@ng-disabled='!authForm.$valid']"));
            saveChange.Click();
            GleamDelay(1000);
            WebElement saveProxy = WaitForElementVisible(driver, By.XPath("//a[@ng-click='applyOptions()']"));
            build.MoveToElement(saveProxy).Build().Perform();
            saveProxy.Click();
            GleamDelay(1000);
        }

        public string FormatedElementUsingXpathClass(string htmlElement,
        string className)
        {
            return ".//" + htmlElement + "[@class='" + className + "')]";
        }

        private void GenerateFolderProfile()
        {
            foreach (Profile profile in listProfile)
            {
                var sourceFolder = "C:\\Users\\hoanm\\AppData\\Local\\Google\\Chrome\\User Data\\" + profile.ProfileName;
                var destFolder = "C:\\Users\\hoanm\\AppData\\Local\\Google\\Chrome\\" + profile.ProfileName + "\\" + profile.ProfileName;
                CopyFilesRecursively(sourceFolder, destFolder);
                Trace.WriteLine("xong profile: " + profile.ProfileName);
            }
        }

        private static void CopyFilesRecursively(string sourcePath, string targetPath)
        {
            //Now Create all of the directories
            foreach (string dirPath in Directory.GetDirectories(sourcePath, "*", SearchOption.AllDirectories))
            {
                Directory.CreateDirectory(dirPath.Replace(sourcePath, targetPath));
            }

            //Copy all the files & Replaces any files with the same name
            foreach (string newPath in Directory.GetFiles(sourcePath, "*.*", SearchOption.AllDirectories))
            {
                File.Copy(newPath, newPath.Replace(sourcePath, targetPath), true);
            }
        }

        public void ExecuteCommand(string Command)
        {
            ProcessStartInfo ProcessInfo;
            Process Process;

            ProcessInfo = new ProcessStartInfo("cmd.exe", "/K " + Command);
            ProcessInfo.CreateNoWindow = true;
            ProcessInfo.UseShellExecute = true;

            Process = Process.Start(ProcessInfo);
        }

        public void ExecuteCommand2(String Command)
        {
            Process[] pname = Process.GetProcessesByName("cmd");
            if (pname.Length != 0)
            {
                // cmd running
                cmd.StandardInput.WriteLine(Command);
            }
            else
            {
                cmd = new Process();
                cmd.StartInfo.FileName = "cmd.exe";
                cmd.StartInfo.RedirectStandardInput = true;
                cmd.StartInfo.RedirectStandardOutput = true;
                cmd.StartInfo.CreateNoWindow = false;
                cmd.StartInfo.UseShellExecute = false;
                cmd.Start();

                cmd.StandardInput.WriteLine(Command);
                //cmd.StandardInput.Flush();
                //cmd.StandardInput.Close();
                //cmd.WaitForExit();
                Trace.WriteLine(cmd.StandardOutput.ReadToEnd());
            }
        }

        static void RunCommands(List<string> cmds, string workingDirectory = "")
        {
            var process = new Process();
            var psi = new ProcessStartInfo();
            psi.FileName = "cmd.exe";
            psi.RedirectStandardInput = true;
            psi.RedirectStandardOutput = true;
            psi.RedirectStandardError = true;
            psi.UseShellExecute = false;
            psi.WorkingDirectory = workingDirectory;
            process.StartInfo = psi;
            process.StartInfo.CreateNoWindow = false;
            process.Start();
            process.OutputDataReceived += (sender, e) => { Console.WriteLine(e.Data); };
            process.ErrorDataReceived += (sender, e) => { Console.WriteLine(e.Data); };
            process.BeginOutputReadLine();
            process.BeginErrorReadLine();
            using (StreamWriter sw = process.StandardInput)
            {
                foreach (var cmd in cmds)
                {
                    sw.WriteLine(cmd);
                }
            }
            process.WaitForExit();
        }
        private void CheckIfNeedToVerifyBrowser(WebDriver driver)
        {
            try
            {
                WaitForPageLoadComplete(driver);
                GleamDelay(3000);
                var hasHcaptcha = driver.FindElements(By.XPath("//textarea[@name='h-captcha-response']")).Count > 0;
                var hasHcaptcha2 = driver.FindElements(By.XPath("//*[contains(text(),'One more step') or contains(text(),'thêm một bước')]")).Count > 0;
                if (hasHcaptcha || hasHcaptcha2)
                {
                    var url = autoData?.Link;
                    if (autoData.UseCookie)
                    {
                        AddCookie(driver);
                        GleamDelay(3000);
                        driver.Navigate().GoToUrl(url);
                    }
                    //driver.Navigate().GoToUrl(url);
                    WaitForPageLoadComplete(driver);
                }
                else
                {
                    Trace.WriteLine("Không tìm thấy Hcaptcha");
                }
                /*driver.Manage().Cookies.DeleteAllCookies();
                driver.Navigate().Refresh();
                GleamDelay(1000);
                WaitForPageLoadComplete(driver);
                var btnLogin = GleamFindElements(By.XPath("//*[@class='fab fa-twitter']"));*/
            }
            catch (Exception e)
            {

            }

        }

        private WebElement GetElementIfExist(WebDriver driver, By byWhat)
        {
            try
            {
                WebElement web = (WebElement)driver.FindElement(byWhat);
                return web;
            }
            catch (Exception e)
            {
                // element not found
            }
            return null;
        }

        private WebElement WaitForElementExists(WebDriver driver, By byWhat)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
            wait.PollingInterval = TimeSpan.FromMilliseconds(500);
            WebElement element = null;
            var timeout = 0;
            Trace.WriteLine("waiting for: " + byWhat.ToString());
            try
            {
                while (isAlertPresent(driver))
                {
                    GleamDelay(1000);
                }
                element = (WebElement)wait.Until(ExpectedConditions.ElementExists(byWhat), Tasks.CancellationTokenSource.Token);
            }
            catch (TaskCanceledException e)
            {
                // ignor
                Trace.WriteLine(e.ToString() + " - WaitForElementVisible");
            }
            catch (Exception e)
            {
                if (Tasks.CancellationTokenSource.IsCancellationRequested)
                {
                    return null;
                }
                // timeout -> repeat check
                Trace.WriteLine(byWhat.ToString() + " - " + Tasks.CancellationTokenSource.IsCancellationRequested);
            }
            return element;
        }

        public bool isAlertPresent(WebDriver driver)
        {
            try
            {
                driver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException ex)
            {
                return false;
            }
        }

        private WebElement WaitForElementVisible(WebDriver driver, By byWhat)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
            wait.PollingInterval = TimeSpan.FromMilliseconds(500);
            WebElement element = null;
            var timeout = 0;
            Trace.WriteLine("waiting for: " + byWhat.ToString());
        label_repeat:
            try
            {
                timeout++;
                while (isAlertPresent(driver))
                {
                    GleamDelay(1000);
                }
                element = (WebElement)wait.Until(ExpectedConditions.ElementIsVisible(byWhat), Tasks.CancellationTokenSource.Token);
            }
            catch (TaskCanceledException e)
            {
                // ignor
                Trace.WriteLine(e.ToString() + " - WaitForElementVisible");
            }
            catch (Exception e)
            {
                if (Tasks.CancellationTokenSource.IsCancellationRequested)
                {
                    return null;
                }
                /*if (e is UnhandledAlertException)
                {
                    Trace.WriteLine("Alert Error: " + e.Message);
                }*/
                // timeout -> repeat check
                Trace.WriteLine(byWhat.ToString() + " - " + Tasks.CancellationTokenSource.IsCancellationRequested);
                if (timeout <= 10)
                {
                    goto label_repeat;
                }
            }
            return element;
        }

        private WebElement WaitForChildElementVisible(WebElement rootElement, By byWhat)
        {
            WebElement childElement = null;
            var timeout = 0;
            Trace.WriteLine("waiting for child: " + byWhat.ToString());
        label_repeat:
            {
                try
                {
                    timeout++;
                    childElement = rootElement.FindElement(byWhat) as WebElement;
                }
                catch (NoSuchElementException e)
                {
                    return childElement;
                }
                catch (Exception e)
                {
                    if (Tasks.CancellationTokenSource.IsCancellationRequested)
                    {
                        return null;
                    }
                    // loading -> repeat check
                    Trace.WriteLine(byWhat.ToString());
                    if (timeout <= 7)
                    {
                        GleamDelay(1000);
                        goto label_repeat;
                    }
                }
            }

            return childElement;
        }

        private List<IWebElement> WaitForElementsVisible(WebDriver driver, By byWhat)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
            wait.PollingInterval = TimeSpan.FromMilliseconds(500);
            List<IWebElement> elements = null;
            Trace.WriteLine("waiting for: " + byWhat.ToString());

            try
            {
                while (isAlertPresent(driver))
                {
                    GleamDelay(1000);
                }
                elements = wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(byWhat)).ToList();
            }
            catch (Exception e)
            {
                // timeout -> repeat check
                Trace.WriteLine(byWhat.ToString() + " - " + e);
            }
            return elements;
        }

        private string FormatedClassName(string className)
        {
            return "." + className.Trim().Replace(" ", ".");
        }

        public void WaitForPageLoadComplete(WebDriver driver)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
            wait.PollingInterval = TimeSpan.FromSeconds(1);
            try
            {
                wait.Until((wdriver) => driver.ExecuteScript("return document.readyState").Equals("complete"), Tasks.CancellationTokenSource.Token);
            }
            catch (WebDriverException e)
            {
                driver.Quit();
                Trace.WriteLine(e.Message);
            }
        }


        private Captcha InitializeCaptchaFactory()
        {
            Captcha captcha = null;
            switch (captchaType)
            {
                case CaptchaType.H_CAPTCHA:
                    captcha = InitializeHcaptcha();
                    break;
                case CaptchaType.NORMAL_CAPTCHA:
                    captcha = InitializeNormalCaptCha();
                    break;
                case CaptchaType.RE_CAPTCHA_V2:
                    captcha = InitializeRecaptchaV2();
                    break;
                case CaptchaType.RE_CAPTCHA_V3:
                    break;

            }
            return captcha;
        }
        private string ResolveCaptCha()
        {
            var captchaCode = "";
            var captcha = InitializeCaptchaFactory();
            try
            {
                while (String.IsNullOrEmpty(captchaCode))
                {
                    Trace.WriteLine("Đang giải captcha: " + captchaType.ToString());
                    solver.Solve(captcha);
                    captchaCode = captcha.Code;
                    Trace.WriteLine("Capt cha: " + captchaCode);
                    Trace.WriteLine("Giải captcha thành công: " + captcha.Code);
                }
            }
            catch (ValidationException e)
            {
                // invalid parameters passed
                Trace.WriteLine("Error occurred: " + e.InnerException.Message);
            }
            catch (NetworkException e)
            {
                // network error occurred
                Trace.WriteLine("Error occurred: " + e.InnerException.Message);
            }
            catch (ApiException e)
            {
                // api respond with error
                Trace.WriteLine("Error occurred: " + e.InnerException.Message);
            }
            catch (TimeoutException e)
            {
                // captcha is not solved so far
                Trace.WriteLine("Error occurred: " + e.InnerException.Message);
            }
            catch (HttpRequestException e)
            {
                // failed to request
                Trace.WriteLine("Error occurred: " + e.InnerException.Message);
            }
            catch (Exception e)
            {
                // unexpected exception
                Trace.WriteLine("Error occurred: " + e.InnerException.Message);
            }
            return captchaCode;
        }

        Normal InitializeNormalCaptCha()
        {
            Normal captcha = new Normal();
            captcha.SetFile("path/to/captcha.jpg");
            captcha.SetNumeric(4);
            captcha.SetMinLen(4);
            captcha.SetMaxLen(20);
            captcha.SetPhrase(true);
            captcha.SetCaseSensitive(true);
            captcha.SetCalc(false);
            captcha.SetLang("en");
            captcha.SetHintImg(new FileInfo("path/to/hint.jpg"));
            captcha.SetHintText("Type red symbols only");
            return captcha;
        }

        ReCaptcha InitializeRecaptchaV2()
        {
            ReCaptcha captcha = new ReCaptcha();
            //captcha.SetSiteKey(edtSiteKey.Text);
            captcha.SetUrl(editlink.Text);
            return captcha;
        }

        HCaptcha InitializeHcaptcha()
        {
            HCaptcha captcha = new HCaptcha();
            captcha.SetSiteKey("2df90a06-8aca-45ee-8ba2-51e9a9113e82");
            captcha.SetUrl(autoData.Link);
            return captcha;
        }

        void ShowProfileList()
        {
            listViewProfile.ItemsSource = GetListProfile();
        }

        void ShowAutoData()
        {

            using var dbContext = new ChromeProfileDbContext();
            var autoData = dbContext.AutoData.ToList().FirstOrDefault();
            if (autoData != null)
            {
                edtChromePath.Text = autoData.ChromePortablePath;
                editlink.Text = autoData.Link;
                editCookiePath.Text = autoData.Cookie;
                CbUseCookie.IsChecked = autoData.UseCookie;
                editProxyId.Text = autoData.ProxyLoginId;
                editProxyPassword.Text = autoData.ProxyLoginPassword;
                editKeyCaptcha.Text = autoData.CaptchaKey;
                this.autoData = autoData;
            }
        }

        string GetRandom3FriendOnTwitter(Profile profile)
        {
            var listTwitterName = new List<String>();
            using var dbContext = new ChromeProfileDbContext();
            var profiles = dbContext.ProfileChrome.ToList().Where(profile => !profile.TweeterName.Equals(profile.TweeterName, StringComparison.OrdinalIgnoreCase)).ToList();
            var friends = "";
            if (profiles.Count > 2)
            {

                Random r = new Random();
                for (int i = 0; i < 3; i++)
                {
                    var index = r.Next(0, profiles.Count);
                    friends += " " + profiles[index].TweeterName;
                }
            }
            else
            {
                friends = "@quynhuu_13 @cz_binance @vucat1989";
            }
            return friends;
        }

        private string[] GetListProxy()
        {
            string[] lines = null;
            try
            {
                lines = System.IO.File.ReadAllLines(@"D:\Crypto\100-IP-UPTO-2022-02-21.txt");

            }
            catch (Exception e)
            {
                // File not found
                Trace.WriteLine("Không tìm thấy file proxy!");
            }
            return lines;
        }

        List<Profile> GetListProfile()
        {
            listProfile.Clear();
            var listProfileChromeDb = ReadChromeProfiles();
            using var dbContext = new ChromeProfileDbContext();
            var rootProfilePath = dbContext.AutoData.ToList().FirstOrDefault();
            if (listProfileChromeDb.Count > 0)
            {
                for (int i = 0; i < listProfileChromeDb.Count; i++)
                {
                    var profileChrome = listProfileChromeDb[i];
                    //profileChrome.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36";
                    Profile profile = new Profile();
                    profile.ID = profileChrome.Id;
                    profile.ProfileName = profileChrome.ProfileName;
                    profile.AbsoluteProfilePath = rootProfilePath != null ? rootProfilePath.ChromePortablePath + "\\" + profileChrome.ProfileName : "";
                    //profile.FullProxy = profileChrome.Proxy;
                    profile.TweeterName = profileChrome.TweeterName;
                    profile.ProfileChrome = profileChrome;
                    profile.FullProxy = profileChrome.Proxy;
                    profile.Status = profileChrome.Status;
                    string[] proxySplit = profileChrome.Proxy.Split(":");
                    if (proxySplit.Count() > 1)
                    {
                        profile.IpProxy = proxySplit[0];
                        profile.PortProxy = proxySplit[1];
                    }
                    listProfile.Add(profile);
                }
            }
            return listProfile;
        }

        List<Profile> getListProfileName()
        {
            listProfile.Clear();
            List<string> profilePaths = getFullPathListProfileName();
            if (profilePaths.Count > 0)
            {
                int maxlen = profilePaths.Max(x => x.Length);
                profilePaths = profilePaths.OrderBy(x => x.PadLeft(maxlen, '0')).ToList();
                string[] lines = GetListProxy();
                for (int i = 0; i < profilePaths.Count; i++)
                {
                    Profile profile = new Profile();
                    profile.ID = i;
                    profile.ProfileName = new DirectoryInfo(profilePaths[i]).Name;
                    profile.AbsoluteProfilePath = profilePaths[i];
                    profile.FullProxy = lines[i];
                    string[] proxySplit = lines[i].Split(":");
                    profile.IpProxy = proxySplit[0];
                    profile.PortProxy = proxySplit[1];
                    listProfile.Add(profile);
                }
            }
            return listProfile;
        }

        static void CreateDbIfNotExist()
        {
            try
            {
                if (!File.Exists("ChromeProfileDB.db"))
                {
                    var dbName = "ChromeProfileDB.db";
                    var dbPath = new FileInfo(dbName).FullName;
                    using var connection = new SQLiteConnection("Data Source=" + dbPath + "; Version = 3;");
                    connection.Open();
                    using (SQLiteCommand fmd = connection.CreateCommand())
                    {
                        fmd.CommandText = File.ReadAllText("D:\\ChromeProfileDB.sql");
                        fmd.ExecuteNonQuery();
                    }
                    connection.Close();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private List<ProfileChrome> ReadChromeProfiles()
        {
            using var dbContext = new ChromeProfileDbContext();
            var profiles = dbContext.ProfileChrome.ToList();
            profiles.ForEach(prf => prf.PrintInfor());
            return profiles;
        }

        public List<string> getFullPathListProfileName()
        {
            List<string> listProfilePath = new List<string>();
            var chromeProfilePath = edtChromePath.Text;
            listProfilePath.AddRange(CustomSearcher.GetDirectories(chromeProfilePath, "GoogleChromePortable *"));
            return listProfilePath;
        }

        private void StopAllThread()
        {
            if (Tasks.CancellationTokenSource != null && Tasks.CancellationTokenSource.IsCancellationRequested == false)
            {
                Tasks.CancellationTokenSource.Cancel();
                Tasks.CancellationTokenSource.Dispose();
            }
        }

        private void ListViewItem_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Profile profile = (sender as ListViewItem).Content as Profile;
            if (profile != null)
            {
                listProfile.Find(x => x.ID == profile.ID).IsSelected = !profile.IsSelected;
            }
        }

        private void ButtonResetStatus_Click(object sender, RoutedEventArgs e)
        {
            foreach (Profile profile in listProfile)
            {
                profile.Status = ProcessStatus.REST.ToString();
            }
            using var dbContext = new ChromeProfileDbContext();
            var profiles = dbContext.ProfileChrome.ToList();
            profiles.ForEach(prf => prf.Status = "REST");
            dbContext.SaveChanges();
        }

        private void ClickSaveAutoData(object sender, RoutedEventArgs e)
        {
            var profilePath = edtChromePath.Text;
            var link = editlink.Text;
            var proxyId = editProxyId.Text;
            var proxyPassword = editProxyPassword.Text;
            var cookiePath = editCookiePath.Text;
            var useCookie = CbUseCookie.IsChecked ?? false;
            var secretCode = edtSecretCode.Text;
            var keyCaptcha = editKeyCaptcha.Text;
            using var dbContext = new ChromeProfileDbContext();
            var listAutoData = dbContext.AutoData.ToList();
            if (listAutoData.Count == 0)
            {
                AutoData autoData = new AutoData();
                autoData.ChromePortablePath = profilePath;
                autoData.Link = link;
                autoData.UseCookie = useCookie;
                autoData.Cookie = cookiePath;
                autoData.ProxyLoginId = proxyId;
                autoData.ProxyLoginPassword = proxyPassword;
                autoData.CaptchaKey = keyCaptcha;
                dbContext.AutoData.Add(autoData);
            }
            else
            {
                var fistAutoData = dbContext.AutoData.ToList().FirstOrDefault();
                fistAutoData.ChromePortablePath = profilePath;
                fistAutoData.Link = link;
                fistAutoData.UseCookie = useCookie;
                fistAutoData.Cookie = cookiePath;
                fistAutoData.ProxyLoginId = proxyId;
                fistAutoData.ProxyLoginPassword = proxyPassword;
                fistAutoData.CaptchaKey = keyCaptcha;
                dbContext.AutoData.Update(fistAutoData);
            }
            dbContext.SaveChanges();
            autoData = dbContext.AutoData.ToList().FirstOrDefault();
            autoData.secretCode = edtSecretCode.Text;
            MessageBox.Show("Success!");
        }

        private void ClickReloadData(object sender, RoutedEventArgs e)
        {
            listProfile = new List<Profile>();
            ShowProfileList();
        }

        private void GleamDelay(int m)
        {
            try
            {
                Task.Delay(m, Tasks.CancellationTokenSource.Token).Wait();
            }
            catch (Exception e)
            {
                Trace.WriteLine(e.Message);
            }
        }
        private void ClickStop(object sender, RoutedEventArgs e)
        {
            if (Tasks.CancellationTokenSource != null && Tasks.CancellationTokenSource.IsCancellationRequested == false)
            {
                Tasks.CancellationTokenSource.Cancel();
                Tasks.CancellationTokenSource.Dispose();
                listProfile.ForEach(profile =>
                {
                    if (profile.DriverService?.ProcessId != default)
                    {
                        try
                        {
                            //profile.NetworkInterceptor.StopMonitoring();
                            profile.Driver.Quit();
                            var nameProcess = Process.GetProcessesByName("chromedriver.exe");
                            var process = Process.GetProcessById(profile.DriverService.ProcessId);
                            var children = GetChildProcesses(profile.DriverService.ProcessId).ToList();
                            children.ForEach(pc => pc.Kill(true));
                            Assert.All(children, p => Assert.True(p.HasExited));
                            Assert.True(process.HasExited);
                        }
                        catch (Exception ex)
                        {
                            Trace.WriteLine("Exception thrown when killing web driver service: " + ex);
                        }
                    }
                });
            }
        }

        private static IEnumerable<Process> GetChildProcesses(int id)
        {
            var children = new List<Process>();
            using var mos = new ManagementObjectSearcher($"Select * From Win32_Process Where ParentProcessID={id}");

            foreach (ManagementBaseObject o in mos.Get())
                try
                {
                    var mo = (ManagementObject)o;
                    var process = Process.GetProcessById(Convert.ToInt32(mo["ProcessID"]));
                    children.Add(process);
                }
                catch (Exception)
                {
                    //ignore
                }

            return children;
        }

        private void BtnSearch_Click(object sender, RoutedEventArgs e)
        {
            var path = editWalletList.Text;
            if (!String.IsNullOrEmpty(path))
            {
                try
                {
                    var text = File.ReadAllLines(path);
                    using var dbContext = new ChromeProfileDbContext();
                    var listProfile = dbContext.ProfileChrome.ToList();
                    var winWallets = "";
                    foreach (var item in listProfile)
                    {
                        if (text.Contains(item.Wallet))
                        {
                            winWallets += item.Wallet + "\n";
                        }
                    }
                    if (winWallets != "")
                    {
                        editWinWallet.Text = winWallets;
                    }
                    else
                    {
                        MessageBox.Show("Không trúng ví nào =)))");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void Click_Kill_All(object sender, RoutedEventArgs e)
        {
            var chromeDriverProcesses = Process.GetProcesses().Where(pr => pr.ProcessName == "chromedriver"); // without '.exe'
            foreach (var process in chromeDriverProcesses)
            {
                process.Kill();
            }
        }

        private void ClickCheckBalance(object sender, RoutedEventArgs e)
        {
            var listWallet = listProfile.Select(x => x.ProfileChrome.Wallet).ToList();
            // 1usd = 296,320,688,219,330.0000 wei
            var wei = 2963206882193300000;
            Task.Run(async () =>
            {

                foreach (var item in listWallet)
                {
                    var reqParams = new RequestParams();
                    reqParams["module"] = "account";
                    reqParams["action"] = "balance";
                    reqParams["address"] = item;
                    reqParams["apikey"] = "PTMGHC8PTUTWSWH94B8GH4BM7WU2Z2FWSK";
                    var get = "https://api.bscscan.com/api";

                    try
                    {
                        var api = "https://api.bscscan.com/api?module=account&action=tokentx&address=" + item + "&page=1&offset=5&startblock=0&endblock=999999999&sort=asc&apikey=PTMGHC8PTUTWSWH94B8GH4BM7WU2Z2FWSK";
                        using var httpClient = new HttpClient();
                        //var responseString = await httpClient.GetStringAsync("https://api.bscscan.com/api?module=account&action=balance&address="+item+"&apikey=PTMGHC8PTUTWSWH94B8GH4BM7WU2Z2FWSK");
                        var responseString = await httpClient.GetStringAsync(api);
                        var obj = System.Text.Json.JsonDocument.Parse(responseString);
                        if (obj.RootElement.TryGetProperty("result", out var result))
                        {
                            /*var usdValue = double.Parse(result.ToString()) / double.Parse("294152447993760");
                            Trace.WriteLine("Wallet: " + item + " - USD: " + usdValue);*/
                            Trace.WriteLine("wallet: " + item + " - " + result);
                        }
                    }
                    catch (Exception ex)
                    {
                        Trace.WriteLine(ex.Message);

                    }
                }
            });
        }

    }
    public class GleamPayload
    {
        public string h { get; set; }
        public string challenge_response { get; set; }
        public bool use_hcaptcha { get; set; } = true;

        public DBG dbg { get; set; }
        public DBGE dbge { get; set; }
        public string f { get; set; }
        public bool rid { get; set; } = false;

        public DETAIL details { get; set; }
    }

    public class GleamPayloadCustomAction
    {
        public string h { get; set; }
        public string challenge_response { get; set; }
        public bool use_hcaptcha { get; set; } = true;

        public DBG dbg { get; set; }
        public DBGE dbge { get; set; }
        public string f { get; set; }
        public bool rid { get; set; } = false;

        public string details { get; set; }
    }

    public class GleamPayloadNonDetails
    {
        public string h { get; set; }
        public string challenge_response { get; set; }
        public bool use_hcaptcha { get; set; } = true;

        public DBG dbg { get; set; }
        public DBGE dbge { get; set; }
        public string f { get; set; }
        public bool rid { get; set; } = false;
    }


    public class DBG
    {
        public EDS eds { get; set; }
        public AFD afd { get; set; }
        public EFD efd { get; set; }
        public bool car { get; set; } = true;


    }

    [JsonConverter(typeof(EDS.Converter))]
    public class EDS
    {
        public Dictionary<string, Entry> eds { get; set; }
        public class Converter : JsonConverter
        {
            public override bool CanConvert(Type type) { return type == typeof(EDS); }

            public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
            {
                throw new NotImplementedException();
            }

            public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
            {
                EDS obj = (EDS)value;
                writer.WriteStartObject();
                foreach (var pair in obj.eds)
                {
                    writer.WritePropertyName(pair.Key.ToString());
                    serializer.Serialize(writer, pair.Value);
                }
                writer.WriteEndObject();
            }
        }
    }
    [JsonConverter(typeof(AFD.Converter))]
    public class AFD
    {
        public Dictionary<string, Entry> afd { get; set; }
        public class Converter : JsonConverter
        {
            public override bool CanConvert(Type type) { return type == typeof(AFD); }

            public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
            {
                throw new NotImplementedException();
            }

            public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
            {
                AFD obj = (AFD)value;
                writer.WriteStartObject();
                foreach (var pair in obj.afd)
                {
                    writer.WritePropertyName(pair.Key.ToString());
                    serializer.Serialize(writer, pair.Value);
                }
                writer.WriteEndObject();
            }
        }
    }
    [JsonConverter(typeof(EFD.Converter))]
    public class EFD
    {
        public Dictionary<string, Entry> efd { get; set; }
        public class Converter : JsonConverter
        {
            public override bool CanConvert(Type type) { return type == typeof(EFD); }

            public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
            {
                throw new NotImplementedException();
            }

            public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
            {
                EFD obj = (EFD)value;
                writer.WriteStartObject();
                foreach (var pair in obj.efd)
                {
                    writer.WritePropertyName(pair.Key.ToString());
                    serializer.Serialize(writer, pair.Value);
                }
                writer.WriteEndObject();
            }
        }
    }
    public class DBGE
    {
        public string eed { get; set; }
        public string hed { get; set; }
        public string csefr { get; set; }
        public string csefn { get; set; }
        public string ae { get; set; }
        public string aebps { get; set; }
        public string re { get; set; }
    }

    public class DETAIL
    {
        public string twitter_username { get; set; }
    }

    public class Entry
    {
        public Entry(string twitterName)
        {
            this.twitter_username = twitterName;
        }
        public string twitter_username { get; set; }

    }
}
