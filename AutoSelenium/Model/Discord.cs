﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoSelenium.Model
{
    class Discord
    {
        public Discord(string login, string password)
        {
            this.login = login;
            this.password = password;
        }
        public string login { get; set; }
        public string password { get; set; }
        public bool undelete { get; set; }
        public string captcha_key { get; set; }
        public string login_source { get; set; }
        public string gift_code_sku_id { get; set; }
    }
}
