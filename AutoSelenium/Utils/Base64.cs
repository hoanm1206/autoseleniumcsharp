﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoSelenium.Utils
{
    class Base64
    {
       public static string GetStringFromBase64String(string msg)
        {

            string _keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

            StringBuilder msgString = new StringBuilder();
            int len = 0;

            while (len < msg.Length)
            {
                int enc1 = _keyStr.IndexOf(msg[len++]);
                int enc2 = _keyStr.IndexOf(msg[len++]);
                int enc3 = _keyStr.IndexOf(msg[len++]);
                int enc4 = _keyStr.IndexOf(msg[len++]);

                int chr1 = (enc1 << 2) | (enc2 >> 4);
                int chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                int chr3 = ((enc3 & 3) << 6) | enc4;

                msgString.Append(Convert.ToChar(chr1));

                if (enc3 != 64)
                {
                    msgString.Append(Convert.ToChar(chr2));
                }
                if (enc4 != 64)
                {
                    msgString.Append(Convert.ToChar(chr3));
                }
            }
            string final = GetUtf8String(msgString.ToString());
            return final;
        }

        public static string GetUtf8String(string msg)
        {
            string utftext = "";
            msg = msg.Replace("\r", "");
            msg = msg.Replace("\n", "");
            msg = msg.Replace("\r\n", "");
            msg = msg.Replace("\r\r", "");
            msg = msg.Replace("\n\n", "");

            msg = msg.Replace("\\r", "");
            msg = msg.Replace("\\n", "");
            msg = msg.Replace("\\r\\n", "");
            msg = msg.Replace("\\r\\r", "");
            msg = msg.Replace("\\n\\n", "");
            for (var n = 0; n < msg.Length; n++)
            {

                int c = msg[n];

                if (c < 128)
                {
                    utftext += Convert.ToChar(c);
                }
                else if ((c > 127) && (c < 2048))
                {
                    utftext += Convert.ToChar((c >> 6) | 192);
                    utftext += Convert.ToChar((c & 63) | 128);
                }
                else
                {
                    utftext += Convert.ToChar((c >> 12) | 224);
                    utftext += Convert.ToChar(((c >> 6) & 63) | 128);
                    utftext += Convert.ToChar((c & 63) | 128);
                }

            }

            return utftext;
        }
    }

}
