﻿using AutoSelenium.Model.DatabaseModel;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Threading.Tasks;
using static AutoSelenium.Utils.Const;

namespace AutoSelenium
{
    [Serializable]
    public class Profile : Bindable
    {
        private bool _IsSelected;

        private string _Status = ProcessStatus.REST.ToString();
        public int ID { get; set; }
        public string ProfileName { get; set; }
        public bool IsSelected { get => _IsSelected; set => Set(ref _IsSelected, value); }
        public string AbsoluteProfilePath { get; set; }
        public string Status { get => _Status; set => Set(ref _Status, value); }
        public string FullProxy { get; set; }
        public string IpProxy { get; set; }

        public string PortProxy { get; set; }

        public string TweeterName { get; set; }
        public string BEP20BSCWallet { get; set; }

        public ProfileChrome ProfileChrome { get; set; }
        public ChromeDriver Driver { get; set; }
        public ChromeDriverService DriverService { get; set; }

        public string entryType { get; set; }
        public string dataTrackEvent { get; set; }

        public bool isCustomDetails { get; set; }
        public string TaskUrl { get; set; }

        public string CaptchaToken { get; set; }
        public bool SimulatorStatus { get; set; }

        public object WindowAlert { get; set; }

        public int CompletableTasks { get; set; }
        public int ResolableTasks { get; set; }
        public int CompletedTasks { get; set; }
        public string ContestantId { get; set; }
        public INetwork NetworkInterceptor { get; set; }
        public int XPosition { get; set; }
        public int YPosition { get; set; }

        public int CurrentPosition { get; set; }

        public int WindownWidth { get; set; }
        public int WindownHeight { get; set; }

    }
    public class EntryType
    {
       /* public static string BANCAMP_FOLLOW = "bandcamp_follow";
        public static string BANCAMP_FOLLOW = "facebook_visit";
        public static string BANCAMP_FOLLOW = "facebook_check_in";
        public static string BANCAMP_FOLLOW = "facebook_view_post";
        public static string BANCAMP_FOLLOW = "messenger_subscribe";
        public static string BANCAMP_FOLLOW = "twitter_follow";
        public static string BANCAMP_FOLLOW = "twitter_retweet";
        public static string BANCAMP_FOLLOW = "twitter_tweet";
        public static string BANCAMP_FOLLOW = "twitter_view_post";
        public static string BANCAMP_FOLLOW = "soundcloud_follow";
        public static string BANCAMP_FOLLOW = "soundcloud_listen";
        public static string BANCAMP_FOLLOW = "soundcloud_like";
        public static string BANCAMP_FOLLOW = "soundcloud_repost";
        public static string BANCAMP_FOLLOW = "spotify_follow";
        public static string BANCAMP_FOLLOW = "spotify_save";
        public static string BANCAMP_FOLLOW = "spotify_listen";
        public static string BANCAMP_FOLLOW = "spotify_presave";
        public static string BANCAMP_FOLLOW = "pinterest_follow";
        public static string BANCAMP_FOLLOW = "pinterest_visit";
        public static string BANCAMP_FOLLOW = "pinterest_pin";
        public static string BANCAMP_FOLLOW = "instagram_visit_profile";
        public static string BANCAMP_FOLLOW = "instagram_view_post";
        public static string BANCAMP_FOLLOW = "twitchtv_follow";
        public static string BANCAMP_FOLLOW = "custom_action";
        public static string BANCAMP_FOLLOW = "tumblr_follow";
        public static string BANCAMP_FOLLOW = "tumblr_like";
        public static string BANCAMP_FOLLOW = "tumblr_reblog";
        public static string BANCAMP_FOLLOW = "eventbrite_attend_event";
        public static string BANCAMP_FOLLOW = "eventbrite_attend_venue";
        public static string BANCAMP_FOLLOW = "steam_play_game";
        public static string BANCAMP_FOLLOW = "steam_join_group";
        public static string BANCAMP_FOLLOW = "goodreads_follow";
        public static string BANCAMP_FOLLOW = "producthunt_follow";
        public static string BANCAMP_FOLLOW = "producthunt_upvote";
        public static string BANCAMP_FOLLOW = "linkedin_follow";
        public static string BANCAMP_FOLLOW = "csv_import";
        public static string BANCAMP_FOLLOW = "snapchat_snapcode";
        public static string BANCAMP_FOLLOW = "youtube_visit_channel";
        public static string BANCAMP_FOLLOW = "patreon_visit";
        public static string BANCAMP_FOLLOW = "reddit_visit";
        public static string BANCAMP_FOLLOW = "telegram_join";*/
    }
}
